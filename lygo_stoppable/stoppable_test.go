package lygo_stoppable

import (
	"testing"
	"time"
)

func TestStoppable(t *testing.T) {
	stoppable := NewStoppable()
	stoppable.Start()
	go func() {
		time.Sleep(10 * time.Second)
		stoppable.Stop()
	}()
	stoppable.Join()
}

func TestStoppableJoinTimeout(t *testing.T) {
	stoppable := NewStoppable()
	stoppable.Start()
	stoppable.JoinTimeout(10*time.Second)
}
