package lygo_stoppable

import (
	"sync"
	"time"
)

type Stoppable struct {
	stopChan chan bool
	mux      sync.Mutex
	waiting  bool
	onStart  func()
	onStop   func()
}

func NewStoppable() *Stoppable {
	instance := new(Stoppable)
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) Start() bool {
	if nil != instance && nil == instance.stopChan {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.stopChan = make(chan bool, 1)
		instance.doStart()

		return true
	}
	return false // not executed
}

func (instance *Stoppable) Stop() bool {
	if nil != instance && nil != instance.stopChan {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.stopChan <- true
		instance.stopChan = nil
		instance.doStop()

		return true
	}
	return false // not executed
}

func (instance *Stoppable) IsStopped() bool {
	if nil != instance && nil != instance.stopChan {
		return false
	}
	return true
}

func (instance *Stoppable) IsJoined() bool {
	return nil != instance && instance.waiting
}

func (instance *Stoppable) Join() {
	if nil != instance && nil != instance.stopChan && !instance.waiting {
		instance.waiting = true

		// wait exit
		<-instance.stopChan
		// reset channel
		instance.stopChan = nil
		instance.waiting = false
	}
}

func (instance *Stoppable) JoinTimeout(d time.Duration) {
	go func() {
		time.Sleep(d)
		instance.Stop()
	}()
	instance.Join()
}

// ---------------------------------------------------------------------------------------------------------------------
//	e v e n t s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) OnStart(f func()) {
	instance.onStart = f
}

func (instance *Stoppable) OnStop(f func()) {
	instance.onStop = f
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) doStop() {
	if nil != instance && nil != instance.onStop {
		instance.onStop()
	}
}

func (instance *Stoppable) doStart() {
	if nil != instance && nil != instance.onStart {
		instance.onStart()
	}
}
