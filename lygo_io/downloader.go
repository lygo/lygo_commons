package lygo_io

import (
	"bitbucket.org/lygo/lygo_commons/lygo_async"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"errors"
	"path/filepath"
	"strings"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	DownloadSession
//----------------------------------------------------------------------------------------------------------------------

type DownloadSession struct {
	pool    *lygo_async.ConcurrentPool
	mux     sync.Mutex
	actions map[string]*DownloaderAction
	files   []string
	errs    []error
}

func NewDownloadSession(actions interface{}) *DownloadSession {
	instance := new(DownloadSession)
	instance.pool = lygo_async.NewConcurrentPool(10)
	instance.files = make([]string, 0)
	instance.errs = make([]error, 0)
	instance.actions = make(map[string]*DownloaderAction)

	if m, b := actions.(map[string]*DownloaderAction); b {
		instance.actions = m
	} else if a, b := actions.([]*DownloaderAction); b {
		for _, v := range a {
			if len(v.Uid) == 0 {
				v.Uid = lygo_rnd.Uuid()
			}
			instance.actions[v.Uid] = v
		}
	}

	return instance
}

func (instance *DownloadSession) DownloadAll(force bool) ([]string, []error) {
	for _, v := range instance.actions {
		source := v.Source
		sourceVersion := v.SourceVersion
		target := v.Target
		// time.Sleep(time.Duration(lygo_rnd.Between(100, 2000)) * time.Millisecond)
		_ = instance.pool.Run(func() error {
			f, e := download(source, sourceVersion, target, force)
			instance.mux.Lock()
			instance.files = append(instance.files, f...)
			instance.mux.Unlock()
			// time.Sleep(time.Duration(lygo_rnd.Between(100, 2000)) * time.Millisecond)
			return e
		})
	}
	pe := instance.pool.Wait()
	if nil != pe && nil != pe.Errors {
		instance.errs = append(instance.errs, pe.Errors...)
	}
	return instance.files, instance.errs
}

//----------------------------------------------------------------------------------------------------------------------
//	Downloader
//----------------------------------------------------------------------------------------------------------------------

type DownloaderAction struct {
	Uid           string `json:"uid"`
	Source        string `json:"source"`
	SourceVersion string `json:"source-version"`
	Target        string `json:"target"`
}

type Downloader struct {
	Actions map[string]*DownloaderAction
}

func NewDownloader() *Downloader {
	instance := new(Downloader)
	instance.Actions = make(map[string]*DownloaderAction)
	return instance
}

func NewAction(uid, source, sourceversion, target string) *DownloaderAction {
	action := new(DownloaderAction)
	if len(uid) == 0 {
		uid = lygo_rnd.Uuid()
	}
	action.Uid = uid
	action.Source = source
	action.SourceVersion = sourceversion
	action.Target = target
	return action
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Downloader) Names() []string {
	response := make([]string, 0, len(instance.Actions))
	for k := range instance.Actions {
		response = append(response, k)
	}
	return response
}

func (instance *Downloader) PutAction(uid, source, sourceversion, target string) *Downloader {
	instance.Put(NewAction(uid, source, sourceversion, target))
	return instance
}

func (instance *Downloader) Put(action *DownloaderAction) *Downloader {
	if nil != action {
		if len(action.Uid) == 0 {
			action.Uid = lygo_rnd.Uuid()
		}
		instance.Actions[action.Uid] = action
	}
	return instance
}

func (instance *Downloader) Download(uid string) (files []string, err error) {
	if v, b := instance.Actions[uid]; b {
		return download(v.Source, v.SourceVersion, v.Target, false)
	}
	return []string{}, nil
}

func (instance *Downloader) ForceDownload(uid string) (files []string, err error) {
	if v, b := instance.Actions[uid]; b {
		return download(v.Source, v.SourceVersion, v.Target, true)
	}
	return
}

func (instance *Downloader) DownloadAll() ([]string, []error) {
	names := instance.Names()
	if len(names) == 1 {
		files, err := instance.Download(names[0])
		if nil != err {
			return files, []error{err}
		}
		return files, []error{}
	} else {
		session := NewDownloadSession(instance.Actions)
		return session.DownloadAll(false)
	}
}

func (instance *Downloader) ForceDownloadAll() ([]string, []error) {
	names := instance.Names()
	if len(names) == 1 {
		files, err := instance.ForceDownload(names[0])
		if nil != err {
			return files, []error{err}
		}
		return files, []error{}
	} else {
		session := NewDownloadSession(instance.Actions)
		return session.DownloadAll(true)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func isZip(source string) bool {
	return strings.ToLower(lygo_paths.ExtensionName(source)) == "zip"
}

func isGreaterThan(rv, lv string) bool {
	rv = strings.Trim(rv, " \n")
	lv = strings.Trim(lv, " \n")
	r := lygo_conv.ToInt(strings.ReplaceAll(rv, ".", ""))
	l := lygo_conv.ToInt(strings.ReplaceAll(lv, ".", ""))
	return r > l
}

func isDownloadable(source, sourceVersion, target string) bool {
	if len(sourceVersion) > 0 {
		// check VERSION
		dir := target
		if b, _ := lygo_paths.IsFile(dir); b {
			dir = lygo_paths.Dir(dir)
		}
		versionFile := lygo_paths.Concat(dir, lygo_paths.FileName(sourceVersion, true))
		data, err := Download(sourceVersion)
		if nil == err {
			remoteVersion := string(data)
			localVersion, err := ReadTextFromFile(versionFile)
			if nil == err {
				return isGreaterThan(remoteVersion, localVersion)
			}
			// write version file
			_, _ = WriteBytesToFile(data, versionFile)
		}
	} else {
		// check EXISTS
		if isZip(source) {
			content, _ := lygo_paths.ReadDir(target)
			return len(content) == 0
		} else {
			targetFile := target
			if b, _ := lygo_paths.IsFile(targetFile); !b {
				targetFile = lygo_paths.Concat(targetFile, lygo_paths.FileName(source, true))
			}
			if b, _ := lygo_paths.Exists(targetFile); b {
				return false
			}
		}
	}
	return true
}

func download(source, sourceversion, target string, force bool) (files []string, err error) {
	target = lygo_paths.Absolute(target)
	dirTarget := mkdir(target)

	if force || isDownloadable(source, sourceversion, target) {
		var bytes []byte
		bytes, err = Download(source)
		if nil != err {
			return nil, err
		}
		if !isValidContent(bytes) {
			return nil, lygo_errors.Prefix(errors.New("invalid_content"), "Download error: ")
		}

		if isZip(source) {
			tmp := lygo_paths.Concat(dirTarget, lygo_rnd.Uuid()+".tmp")
			defer remove(tmp)
			_, err = WriteBytesToFile(bytes, tmp)
			if nil == err {
				files, err = Unzip(tmp, dirTarget)
			}
		} else {
			targetFile := target
			if b, _ := lygo_paths.IsFile(targetFile); !b {
				targetFile = lygo_paths.Concat(targetFile, lygo_paths.FileName(source, true))
			}

			// write a file
			_, err = WriteBytesToFile(bytes, targetFile)
			files = append(files, target)
		}
		return
	}
	return
}

func remove(path string) {
	err := Remove(path)
	if nil == err {
		parent := lygo_paths.Dir(path)
		files, _ := lygo_paths.ReadDir(parent)
		if len(files) == 0 {
			_ = RemoveAll(parent)
		}
	}
}

func mkdir(path string) string {
	if len(filepath.Ext(path)) > 0 {
		_ = lygo_paths.Mkdir(path)
		return lygo_paths.Dir(path)
	} else {
		_ = lygo_paths.Mkdir(lygo_paths.ConcatDir(path, lygo_paths.OS_PATH_SEPARATOR))
		return path
	}
}

func isValidContent(bytes []byte) bool {
	if len(bytes) > 10 {
		start := strings.TrimSpace(string(bytes[:10]))
		if strings.HasPrefix(start, "<!") {
			// is html. check html is not a bitbucket error
			content := string(bytes)
			if strings.Index(content, "That link has no power here") > -1 {
				return false
			}
		}
	}
	return true
}
