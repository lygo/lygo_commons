package lygo_io

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func FileSize(filename string) (int64, error) {
	info, err := os.Stat(filename)
	if nil != err {
		return 0, err
	}
	return info.Size(), nil
}

func Remove(filename string) error {
	return os.Remove(filename)
}

func RemoveAll(path string) error {
	return os.RemoveAll(path)
}

func RemoveSilent(filename string) {
	_ = os.Remove(filename)
}

func RemoveAllSilent(path string) {
	_ = os.RemoveAll(path)
}

func MoveFile(from, to string) error {
	if b, _ := lygo_paths.IsFile(to); !b {
		to = lygo_paths.Concat(to, lygo_paths.FileName(from, true))
	}
	_, err := CopyFile(from, to)
	if nil != err {
		return err
	}
	return Remove(from)
}

func CopyFile(src, dst string) (int64, error) {
	lygo_paths.Mkdir(dst)

	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func AppendTextToFile(text, file string) (bytes int, err error) {
	var f *os.File
	if b, _ := lygo_paths.Exists(file); b {
		f, err = os.OpenFile(file,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	} else {
		f, err = os.Create(file)
	}

	if nil == err {
		defer f.Close()
		w := bufio.NewWriter(f)
		bytes, err = w.WriteString(text)
		w.Flush()
	}
	return bytes, err
}

func WriteTextToFile(text, file string) (bytes int, err error) {
	var f *os.File
	f, err = os.Create(file)

	if nil == err {
		defer f.Close()
		w := bufio.NewWriter(f)
		bytes, err = w.WriteString(text)
		w.Flush()
	}
	return bytes, err
}

func WriteBytesToFile(data []byte, file string) (bytes int, err error) {
	var f *os.File
	f, err = os.Create(file)
	if nil == err {
		defer f.Close()
		w := bufio.NewWriter(f)
		bytes, err = w.Write(data)
		w.Flush()
	}
	return bytes, err
}

func ReadBytesFromFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	return b, err
}

func ReadTextFromFile(fileName string) (string, error) {
	b, err := ReadBytesFromFile(fileName)
	return string(b), err
}

func Download(url string) ([]byte, error) {
	if len(url) > 0 {
		if strings.Index(url, "http") > -1 {
			// HTTP
			tr := &http.Transport{
				MaxIdleConns:       10,
				IdleConnTimeout:    15 * time.Second,
				DisableCompression: true,
			}
			client := &http.Client{Transport: tr}
			resp, err := client.Get(url)
			if nil == err {
				defer resp.Body.Close()
				body, err := ioutil.ReadAll(resp.Body)
				if nil == err {
					return body, nil
				} else {
					return []byte{}, err
				}
			} else {
				return []byte{}, err
			}
		} else {
			// FILE SYSTEM
			return ReadBytesFromFile(url)
		}
	}
	return []byte{}, lygo_errors.Prefix(errors.New("missing_url"), "Missing Parameter 'url': ")
}

func ReadHashFromFile(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}

// ScanBytesFromFile read a file line by line
func ScanBytesFromFile(fileName string, callback func(data []byte) bool) error {
	if nil == callback {
		return errors.New("missing_callback")
	}
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if callback(scanner.Bytes()) {
			// exit loop
			return nil
		}
	}

	return scanner.Err()
}

// ScanTextFromFile read a text file line by line
func ScanTextFromFile(fileName string, callback func(text string) bool) error {
	if nil == callback {
		return errors.New("missing_callback")
	}
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if callback(scanner.Text()) {
			// exit loop
			return nil
		}
	}

	return scanner.Err()
}

func ReadAllBytes(reader io.Reader) ([]byte, error) {
	return ioutil.ReadAll(reader)
}

func ReadAllString(reader io.Reader) (string, error) {
	buf, err := ioutil.ReadAll(reader)
	if nil != err {
		return "", err
	}
	return string(buf), nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
