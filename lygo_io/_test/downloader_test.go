package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"fmt"
	"testing"
)

func TestDownloader(t *testing.T) {
	target := "./dir-download"
	defer lygo_io.RemoveAll(target)

	downloader := lygo_io.NewDownloader().
		PutAction("bitbucket_1",
			"https://bitbucket.org/lygo/lygo_app_guardian_ide/raw/master/_build/www/a-webui.zip",
			"https://bitbucket.org/lygo/lygo_app_guardian_ide/raw/master/_build/build_version.txt",
		target).
		PutAction("",
			"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian",
			"",
		target).
		PutAction("",
			"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian",
			"",
		target + "/g").
		PutAction("",
			"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian",
			"",
		target + "/gg")

	files, errs := downloader.DownloadAll()
	if len(errs) > 0 {
		t.Error(errs[0])
		t.FailNow()
	}
	fmt.Println("Downloaded:", len(files), "files")

	// test existing file
	downloader = lygo_io.NewDownloader().
		PutAction("",
			"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian",
			"",
		target + "/gg")
	files, errs = downloader.DownloadAll()
	if len(errs) > 0 {
		t.Error(errs[0])
		t.FailNow()
	}
	if len(files)>0{
		t.Error("Expected no files to download")
		t.FailNow()
	}
	fmt.Println("Downloaded (not forced):", len(files), "files")

	// test existing file with force
	downloader = lygo_io.NewDownloader().
		PutAction("",
			"https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian",
			"",
			target + "/gg")
	files, errs = downloader.ForceDownloadAll()
	if len(errs) > 0 {
		t.Error(errs[0])
		t.FailNow()
	}
	if len(files)==0{
		t.Error("Expected 1 file to download")
		t.FailNow()
	}
	fmt.Println("Downloaded (forced):", len(files), "files")
}

func TestDownloaderInvalid(t *testing.T) {
	target := "./dir-download"
	defer lygo_io.RemoveAll(target)

	downloader := lygo_io.NewDownloader().
		PutAction("bitbucket_1",
			"https://bitbucket.org/lygo/lygo_app_guardian_ide/raw/master/_build/www/a-webui.zippo",
			"", target)

	files, errs := downloader.DownloadAll()
	if len(errs) == 0 {
		t.Error("Expected error")
		t.FailNow()
	}
	fmt.Println("Downloaded:", len(files), "files")

}