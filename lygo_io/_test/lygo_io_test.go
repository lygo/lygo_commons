package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"testing"
)

func TestHash(t *testing.T) {
	source := lygo_paths.Absolute("./file.txt")
	for i:=0;i<100000;i++{
		s, err := lygo_io.ReadHashFromFile(source)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		if s!="c4665146b493b2c7223efcdf13a3d7a1046b3ad631d56f89e5824e8a598d5b3f"{
			t.Error("Wrong read, expected 'c4665146b493b2c7223efcdf13a3d7a1046b3ad631d56f89e5824e8a598d5b3f'", "got", s)
			t.FailNow()
		}
	}
}

// read a file for thousand cycles
func TestRead(t *testing.T) {
	source := lygo_paths.Absolute("./file.txt")
	for i:=0;i<100000;i++{
		s, err := lygo_io.ReadTextFromFile(source)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		if s!="Hello, I'm a simple file"{
			t.Error("Wrong read, expected 'Hello, I'm a simple file'")
			t.FailNow()
		}
	}
}

func TestReadLines(t *testing.T) {
	readLines := ""
	source := lygo_paths.Absolute("./file.txt")
	count := 0
	err := lygo_io.ScanTextFromFile(source, func(text string) bool {
		readLines += text + "\n"
		count++
		return count>1 // read only 2 lines
	})
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	if len(readLines)==0{
		t.Error("Nothing to read")
		t.FailNow()
	}
	fmt.Println(readLines)
}

func TestWrite(t *testing.T) {
	text := "Hello, I'm a simple file"
	source := lygo_paths.Absolute("./file.txt")
	for i:=0;i<100000;i++{
		s, err := lygo_io.ReadTextFromFile(source)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		if s!=text{
			t.Error("Wrong read, expected 'Hello, I'm a simple file'")
			t.FailNow()
		}

		_, err = lygo_io.WriteTextToFile(text, source)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
	}
}

// move and read a file for thousand cycles
func TestMove(t *testing.T) {
	source := lygo_paths.Absolute("./file.txt")
	dir1 := lygo_paths.Absolute("./dir1/file.txt")
	dir2 := lygo_paths.Absolute("./dir2/file.txt")
	if b, _ := lygo_paths.Exists(dir1);!b{
		err := lygo_paths.Mkdir(dir1)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		err = lygo_paths.Mkdir(dir2)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		_, err = lygo_io.CopyFile(source, dir1)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
	}
	for i:=0;i<10000;i++{
		_, err := lygo_io.CopyFile(dir1, dir2)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}

		s, err := lygo_io.ReadTextFromFile(dir2)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
		if s!="Hello, I'm a simple file"{
			t.Error("Wrong read, expected 'Hello, I'm a simple file'")
			t.FailNow()
		}

		err = lygo_io.Remove(dir2)
		if nil!=err{
			t.Error(err)
			t.FailNow()
		}
	}
	err := lygo_io.Remove(dir1)
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
}
