package lygo_sys

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_mu"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"syscall"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type InfoObject struct {
	GoOS        string `json:"goos"`
	Kernel      string `json:"kernel"`
	Core        string `json:"core"`
	Platform    string `json:"platform"`
	OS          string `json:"os"`
	Hostname    string `json:"hostname"`
	CPUs        int    `json:"cpus"`
	MemoryUsage string `json:"memory_usage"`
}

func (instance *InfoObject) VarDump() {
	fmt.Println("GoOS:", instance.GoOS)
	fmt.Println("Kernel:", instance.Kernel)
	fmt.Println("Core:", instance.Core)
	fmt.Println("Platform:", instance.Platform)
	fmt.Println("OS:", instance.OS)
	fmt.Println("Hostname:", instance.Hostname)
	fmt.Println("CPUs:", instance.CPUs)
	fmt.Println("MemoryUsage:", instance.MemoryUsage)
}

func (instance *InfoObject) ToString() string {
	return fmt.Sprintf("GoOS:%v,Kernel:%v,Core:%v,Platform:%v,OS:%v,Hostname:%v,CPUs:%v, MemoryUsage:%v", instance.GoOS, instance.Kernel, instance.Core, instance.Platform, instance.OS, instance.Hostname, instance.CPUs, instance.MemoryUsage)
}

func (instance *InfoObject) ToJsonString() string {
	return lygo_json.Stringify(instance)
}

type MemObject struct {
	Alloc      uint64 `json:"alloc"`
	TotalAlloc uint64 `json:"total_alloc"`
	Sys        uint64 `json:"sys"`
	NumGC      uint32 `json:"num_gc"`
	HeapSys    uint64 `json:"heap_sys"`
}

func NewMemoryUsageInfo() *MemObject {
	instance := new(MemObject)
	// memory
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	instance.Alloc = m.Alloc
	instance.TotalAlloc = m.TotalAlloc
	instance.Sys = m.Sys
	instance.NumGC = m.NumGC
	instance.HeapSys = m.HeapSys

	return instance
}

func (instance *MemObject) ToString() string {
	return fmt.Sprintf("Alloc = %v, TotalAlloc = %v, Sys = %v, NumGC = %v, HeapSys = %v",
		lygo_mu.FmtBytes(instance.Alloc), lygo_mu.FmtBytes(instance.TotalAlloc), lygo_mu.FmtBytes(instance.Sys), instance.NumGC,
		lygo_mu.FmtBytes(instance.HeapSys))

}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func GetInfo() *InfoObject {
	info := getInfo()

	// memory
	info.MemoryUsage = NewMemoryUsageInfo().ToString()

	return info
}

func GetOS() string {
	return runtime.GOOS
}

func IsMac() bool {
	return "darwin" == GetOS()
}

func IsLinux() bool {
	return "linux" == GetOS()
}

func IsWindows() bool {
	return "windows" == GetOS()
}

func GetOSVersion() string {
	return GetInfo().Core
}

// shutdown the machine
func Shutdown(a ...string) error {
	adminPsw := ""
	if len(a) == 1 {
		adminPsw = a[0]
	}
	return shutdown(adminPsw)
}

// ID returns the platform specific machine id of the current host OS.
// Regard the returned id as "confidential" and consider using ProtectedID() instead.
// THANKS TO: github.com/denisbrodbeck/machineid
func ID() (string, error) {
	id, err := machineID()
	if err != nil {
		return "", fmt.Errorf("machineid: %v", err)
	}
	return id, nil
}

// ProtectedID returns a hashed version of the machine ID in a cryptographically secure way,
// using a fixed, application-specific key.
// Internally, this function calculates HMAC-SHA256 of the application ID, keyed by the machine ID.
// THANKS TO: github.com/denisbrodbeck/machineid
func ProtectedID(appID string) (string, error) {
	id, err := ID()
	if err != nil {
		return "", fmt.Errorf("machineid: %v", err)
	}
	return protect(appID, id), nil
}

func FindCurrentProcess() (*os.Process, error) {
	return os.FindProcess(syscall.Getpid())
}

func KillCurrentProcess() error {
	p, err := FindCurrentProcess()
	if nil != err {
		return err
	}
	return p.Signal(os.Interrupt)
}

func KillProcessByPid(pid int) error {
	p, err := os.FindProcess(pid)
	if nil != err {
		return err
	}
	err = p.Kill()
	if nil != err {
		// process found but already closed?
	}
	return err
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

// run wraps `exec.Command` with easy access to stdout and stderr.
func run(stdout, stderr io.Writer, cmd string, args ...string) error {
	c := exec.Command(cmd, args...)
	c.Stdin = os.Stdin
	c.Stdout = stdout
	c.Stderr = stderr
	return c.Run()
}

// protect calculates HMAC-SHA256 of the application ID, keyed by the machine ID and returns a hex-encoded string.
func protect(appID, id string) string {
	mac := hmac.New(sha256.New, []byte(id))
	mac.Write([]byte(appID))
	return hex.EncodeToString(mac.Sum(nil))
}

func readFile(filename string) ([]byte, error) {
	return ioutil.ReadFile(filename)
}

func trim(s string) string {
	return strings.TrimSpace(strings.Trim(s, "\n"))
}
