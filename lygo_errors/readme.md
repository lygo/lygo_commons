# LyGo Errors

This package has been inspired from original 
[go-lygo_errors](https://github.com/hashicorp/go-lygo_errors) awesome package.

Extends "error" supporting multiple errors.

The package provides a mechanism for representing a list of error values as a single error.

## Usage

### Building a list of errors

The Append function is used to create a list of errors. 
This function behaves a lot like the Go built-in append 
function: it doesn't matter if the first argument is nil, 
a lygo_errors.Error, or any other error, the function behaves 
as you would expect.

```
var result error

if err := step1(); err != nil {
	result = lygo_errors.Append(result, err)
}
if err := step2(); err != nil {
	result = lygo_errors.Append(result, err)
}

return result
```

### Customizing the formatting of the errors

By specifying a custom ErrorFormat, you can customize the format of the Error() string function:

```
var result *lygo_errors.Error

// ... accumulate errors here, maybe using Append

if result != nil {
	result.ErrorFormat = func([]error) string {
		return "errors!"
	}
}
```

### Accessing the list of errors

lygo_errors.Error implements error so if the caller 
doesn't know about lygo_errors, it will work just fine. 
But if you're aware a lygo_errors might be returned, you can 
use type switches to access the list of errors:

```
if err := something(); err != nil {
	if es, ok := err.(*lygo_errors.Error); ok {
		// Use es.Errors
	}
}
```

You can also use the standard errors.Unwrap function. This will continue to unwrap into subsequent errors until none exist.

### Extracting an error

The standard library errors.As function can be used directly with a lygo_errors to extract a specific error:

```
// Assume err is a lygo_errors value
err := somefunc()

// We want to know if "err" has a "RichErrorType" in it and extract it.
var errRich RichErrorType
if errors.As(err, &errRich) {
	// It has it, and now errRich is populated.
}
```

### Checking for an exact error value

Some errors are returned as exact errors such as the ErrNotExist error in the os package. You can check if this error is present by using the standard errors.Is function.

```
// Assume err is a lygo_errors value
err := somefunc()
if errors.Is(err, os.ErrNotExist) {
	// err contains os.ErrNotExist
}
```

### Returning a lygo_errors only if there are errors

If you build a lygo_errors.Error, you can use the ErrorOrNil function to return an error implementation only if there are errors to return:

```
var result *lygo_errors.Error

// ... accumulate errors here

// Return the `error` only if errors were added to the lygo_errors, otherwise
// return nil since there are no errors.
return result.ErrorOrNil()
```
