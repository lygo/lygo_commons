package _test_test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func TestErrorImpl(t *testing.T) {
	var _ error = new(lygo_errors.Error)
}

func TestErrorPrefix(t *testing.T) {
	err := errors.New("test err")
	perr := lygo_errors.Prefix(err, "prefixed_error")
	if nil!=perr && perr.Error()!="prefixed_error test err"{
		t.Fatalf("bad: %s", perr)
	}
}

func TestErrorError_custom(t *testing.T) {
	errorList := []error{
		errors.New("foo"),
		errors.New("bar"),
	}

	fn := func(es []error) string {
		return "foo"
	}

	multi := &lygo_errors.Error{Errors: errorList, ErrorFormat: fn}
	if multi.Error() != "foo" {
		t.Fatalf("bad: %s", multi.Error())
	}
}

func TestErrorError_default(t *testing.T) {
	expected := `2 errors occurred:
	* foo
	* bar`

	errorList := []error{
		errors.New("foo"),
		errors.New("bar"),
	}

	multi := &lygo_errors.Error{Errors: errorList}
	multis := strings.Trim(multi.Error(), "\n")
	if multis != expected {
		t.Fatalf("bad: %s", multis)
	}
}

func TestErrorErrorOrNil(t *testing.T) {
	err := new(lygo_errors.Error)
	if err.ErrorOrNil() != nil {
		t.Fatalf("bad: %#v", err.ErrorOrNil())
	}

	err.Errors = []error{errors.New("foo")}
	if v := err.ErrorOrNil(); v == nil {
		t.Fatal("should not be nil")
	} else if !reflect.DeepEqual(v, err) {
		t.Fatalf("bad: %#v", v)
	}
}

func TestErrorWrappedErrors(t *testing.T) {
	errorList := []error{
		errors.New("foo"),
		errors.New("bar"),
	}

	multi := &lygo_errors.Error{Errors: errorList}
	if !reflect.DeepEqual(multi.Errors, multi.WrappedErrors()) {
		t.Fatalf("bad: %s", multi.WrappedErrors())
	}
}

func TestErrorUnwrap(t *testing.T) {
	t.Run("with errors", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			errors.New("bar"),
			errors.New("baz"),
		}}

		var current error = err
		for i := 0; i < len(err.Errors); i++ {
			current = errors.Unwrap(current)
			if !errors.Is(current, err.Errors[i]) {
				t.Fatal("should be next value")
			}
		}

		if errors.Unwrap(current) != nil {
			t.Fatal("should be nil at the end")
		}
	})

	t.Run("with no errors", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: nil}
		if errors.Unwrap(err) != nil {
			t.Fatal("should be nil")
		}
	})

	t.Run("with nil lygo_errors", func(t *testing.T) {
		var err *lygo_errors.Error
		if errors.Unwrap(err) != nil {
			t.Fatal("should be nil")
		}
	})
}

func TestErrorIs(t *testing.T) {
	errBar := errors.New("bar")

	t.Run("with errBar", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			errBar,
			errors.New("baz"),
		}}

		if !errors.Is(err, errBar) {
			t.Fatal("should be true")
		}
	})

	t.Run("with errBar wrapped by fmt.Errorf", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			fmt.Errorf("errorf: %w", errBar),
			errors.New("baz"),
		}}

		if !errors.Is(err, errBar) {
			t.Fatal("should be true")
		}
	})

	t.Run("without errBar", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			errors.New("baz"),
		}}

		if errors.Is(err, errBar) {
			t.Fatal("should be false")
		}
	})
}

func TestErrorAs(t *testing.T) {
	match := &nestedError{}

	t.Run("with the value", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			match,
			errors.New("baz"),
		}}

		var target *nestedError
		if !errors.As(err, &target) {
			t.Fatal("should be true")
		}
		if target == nil {
			t.Fatal("target should not be nil")
		}
	})

	t.Run("with the value wrapped by fmt.Errorf", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			fmt.Errorf("errorf: %w", match),
			errors.New("baz"),
		}}

		var target *nestedError
		if !errors.As(err, &target) {
			t.Fatal("should be true")
		}
		if target == nil {
			t.Fatal("target should not be nil")
		}
	})

	t.Run("without the value", func(t *testing.T) {
		err := &lygo_errors.Error{Errors: []error{
			errors.New("foo"),
			errors.New("baz"),
		}}

		var target *nestedError
		if errors.As(err, &target) {
			t.Fatal("should be false")
		}
		if target != nil {
			t.Fatal("target should be nil")
		}
	})
}

// nestedError implements error and is used for tests.
type nestedError struct{}

func (*nestedError) Error() string { return "" }
