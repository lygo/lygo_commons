module bitbucket.org/lygo/lygo_commons

go 1.14

require (
	github.com/google/uuid v1.2.0
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887
)
