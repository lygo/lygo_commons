package lygo_crypto

import (
	"encoding/base64"
)

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

// Deprecated: EncodeBase64 is deprecated. Use lygo_coding.EncodeBase64
func EncodeBase64(data []byte) string {
	return base64.StdEncoding.EncodeToString(data)
}

// Deprecated: DecodeBase64 is deprecated. Use lygo_coding.DecodeBase64
func DecodeBase64(data string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(data)
}
