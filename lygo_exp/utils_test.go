package lygo_exp

import (
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"fmt"
	"strings"
	"testing"
)

func TestParseStatement(t *testing.T) {
	phrase := "A + B (C + (D + E +(F&J))-K) + (Z|X)"
	normalized := Normalize(phrase)
	fmt.Println("PHRASE:\t\t", phrase)
	fmt.Println("NORMALIZED:\t", normalized)
	statements, err := ParseStatement(phrase)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	expected := "(A+B)|(C+(D+E)+(F&J)-K)+(Z|X)"
	got := statements.String()
	if got != expected {
		t.Error("Expected: ", "'"+expected+"'", "but got ", "'"+got+"'")
		t.FailNow()
	}
	fmt.Println("RESULT:\t\t", got)

	fmt.Println("EXPLAINED")
	fmt.Println(statements.Explain())
}

func TestWalkThrough(t *testing.T) {
	phrase := "A + B (C + (D + E +(F&J))-K) + (Z|X)"
	normalized := Normalize(phrase)
	fmt.Println("NORMALIZED:\t", normalized)
	statements, err := ParseStatement(phrase)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// walk
	statements.WalkThrough(func(level, index int, statement *Statement) {
		indent := lygo_strings.Repeat("\t", level)
		fmt.Println(fmt.Sprintf("%s%s %s [is-block:%v, is-branch:%v]", indent, statement.Uid, statement.String(), statement.IsBlock(), statement.IsBranch()))
	})
}

func TestIsBalanced(t *testing.T) {
	b := IsBalanced("(sss)(sss)")
	if !b {
		t.FailNow()
	}

	b = IsBalanced("((sss)(sss)")
	if b {
		t.FailNow()
	}
}

func TestSplitSibling(t *testing.T) {
	phrase := "A + B (C + (D + E +(F&J))-K) + (Z|X)"
	tokens := SplitSibling(phrase)
	if len(tokens) != 3 {
		t.Error("Expected 3 siblings")
		t.FailNow()
	}
	s := strings.Join(tokens, ",")
	expected := "A+B|,C+(D+E+(F&J))-K+,Z|X"
	if s != expected {
		t.Error("Expected: ", "'"+expected+"'", "but got ", "'"+s+"'")
		t.FailNow()
	}
	fmt.Println(s)
}

func TestNormalize(t *testing.T) {
	phrase := "A + B (C + (D + E +(F&J))) + Z"
	fmt.Println("NORMALIZE: ", phrase)
	s := Normalize(phrase)
	if s != "A+B|(C+(D+E+(F&J)))+Z" {
		t.Error("Expected: 'A+B|(C+(D+E+(F&J)))+Z'", "but got ", "'"+s+"'")
		t.FailNow()
	}
	fmt.Println(s)
}
