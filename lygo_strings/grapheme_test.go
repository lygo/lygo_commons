package lygo_strings

import (
	"fmt"
	"testing"
)

func TestExampleGraphemes(t *testing.T) {
	gr := NewGraphemes("👍🏼!")
	for gr.Next() {
		fmt.Printf("%x ", gr.Runes())
	}
	// Output: [1f44d 1f3fc] [21]
}

func TestExampleGraphemeClusterCount(t *testing.T) {
	n := GraphemeClusterCount("🇩🇪🏳️‍🌈")
	if n != 2 {
		t.Error("Expected 2, got ", n)
		t.FailNow()
	}
	fmt.Println(n)
	// Output: 2
}
