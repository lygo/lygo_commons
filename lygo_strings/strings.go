package lygo_strings

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bytes"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const escape = '\\'

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func RemoveDuplicateSpaces(text string) string {
	space := regexp.MustCompile(`\s+`) // [\s\p{Zs}]{2,}
	return space.ReplaceAllString(text, " ")
}

func TrimSpaces(slice []string) {
	Trim(slice, " ")
}

func Trim(slice []string, trimVal string) {
	for i := range slice {
		slice[i] = strings.Trim(slice[i], trimVal)
	}
}

func Clear(text string) string {
	var buf bytes.Buffer
	lines := strings.Split(text, "\n")
	count := 0
	for _, line := range lines {
		space := regexp.MustCompile(`\s+`)
		s := strings.TrimSpace(space.ReplaceAllString(line, " "))
		if len(s) > 0 {
			if count > 0 {
				buf.WriteString("\n")
			}
			buf.WriteString(strings.TrimSpace(s))
			count++
		}
	}
	return buf.String()
}

//Paginate Clear and paginate text on multiple rows.
// New rows only if prev char is a dot (.)
func Paginate(text string) string {
	// split rows
	lines := strings.Split(Clear(text), ".\n")
	var buf bytes.Buffer
	count := 0
	for _, line := range lines {
		if count > 0 {
			buf.WriteString(".\n")
		}
		buf.WriteString(strings.ReplaceAll(line, "\n", " "))
		count++
	}
	return buf.String()
}

func Concat(params ...interface{}) string {
	result := ""
	for _, v := range params {
		result += lygo_conv.ToString(v)
	}
	return result
}

func ConcatSep(separator string, params ...interface{}) string {
	result := ""
	strParams := lygo_conv.ToArrayOfString(params...)
	for _, value := range strParams {
		if len(result) > 0 {
			result += separator
		}
		result += value
	}
	return result
}

func ConcatTrimSep(separator string, params ...interface{}) string {
	result := ""
	for _, v := range params {
		value := strings.TrimSpace(lygo_conv.ToString(v))
		if len(value) > 0 {
			if len(result) > 0 {
				result += separator
			}
			result += value
		}
	}
	return result
}

func Format(s string, params ...interface{}) string {
	return fmt.Sprintf(strings.Replace(s, "%s", "%v", -1), params...)
}

func FormatValues(s string, params ...interface{}) string {
	return fmt.Sprintf(s, params...)
}

// Split using all rune in a string of separators
func Split(s string, seps string) []string {
	return strings.FieldsFunc(s, func(r rune) bool {
		for _, sep := range seps {
			if r == sep {
				return true
			}
		}
		return false
	})
}

func SplitAfter(s string, seps string) (tokens []string, separators []string) {
	tokens = strings.FieldsFunc(s, func(r rune) bool {
		for _, sep := range seps {
			if r == sep {
				separators = append(separators, string(sep))
				return true
			}
		}
		return false
	})
	return
}

func SplitTrim(s string, seps string, cutset string) []string {
	data := Split(s, seps)
	for i, item := range data {
		data[i] = strings.Trim(item, cutset)
	}
	return data
}

func SplitTrimSpace(s string, seps string) []string {
	return SplitTrim(s, seps, " ")
}

func SplitAndGetAt(s string, seps string, index int) string {
	tokens := Split(s, seps)
	if len(tokens) > index {
		return tokens[index]
	}
	return ""
}

func SplitLast(s string, sep rune) []string {
	data := strings.Split(s, string(sep))
	if len(data) > 1 {
		return []string{strings.Join(data[:len(data)-1], string(sep)), data[len(data)-1]}
	}
	return data
}

// SplitQuoted splits a string, ignoring separators present inside of quoted runs.  Separators
// cannot be escaped outside of quoted runs, the escaping will be ignored.
//
// Quotes are preserved in result, but the separators are removed.
func SplitQuoted(s string, sep rune, quote rune) []string {
	a := make([]string, 0, 8)
	quoted := false
	escaped := false
	p := 0
	for i, c := range s {
		if c == escape {
			// Escape can escape itself.
			escaped = !escaped
			continue
		}
		if c == quote {
			quoted = !quoted
			continue
		}
		escaped = false
		if !quoted && c == sep {
			a = append(a, s[p:i])
			p = i + 1
		}
	}

	if quoted && quote != 0 {
		// s contained an unterminated quoted-run, re-split without quoting.
		return SplitQuoted(s, sep, rune(0))
	}

	return append(a, s[p:])
}


// Sub get a substring
// @param s string The string
// @param start int Start index
// @param end int End index
func Sub(s string, start int, end int) string {
	runes := []rune(s) // convert in rune to handle all characters.
	if start < 0 || start > end {
		start = 0
	}
	if end > len(runes) {
		end = len(runes)
	}

	return string(runes[start:end])
}

func Contains(s string, seps string) bool {
	for _, r := range seps {
		if strings.Index(s, string(r)) > -1 {
			return true
		}
	}
	return false
}

//----------------------------------------------------------------------------------------------------------------------
//	n o r m a l i z a t i o n
//----------------------------------------------------------------------------------------------------------------------

func Slugify(text string, replaces ...string) string {
	// remove duplicate spaces, carriage returns and tabs
	space := regexp.MustCompile(`\s+`)
	text = space.ReplaceAllString(text, " ")
	if len(replaces) > 0 {
		for _, replace := range replaces {
			if len(replace) > 1 && strings.Index(replace, ":") > -1 {
				tokens := strings.Split(replace, ":")
				text = strings.ReplaceAll(text, tokens[0], tokens[1])
			} else {
				text = strings.ReplaceAll(text, " ", replace)
			}
		}
		return text
	} else {
		return strings.ReplaceAll(text, " ", "-")
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	C a m e l    C a s e
//----------------------------------------------------------------------------------------------------------------------

func CapitalizeAll(text string) string {
	return strings.Title(text)
}

func CapitalizeFirst(text string) string {
	if len(text) > 0 {
		words := Split(text, " ")
		if len(words) > 0 {
			words[0] = strings.Title(words[0])
			return ConcatSep(" ", words)
		}
	}
	return text
}

//----------------------------------------------------------------------------------------------------------------------
//	p a d d i n g
//----------------------------------------------------------------------------------------------------------------------

func FillLeft(text string, l int, r rune) string {
	if len(text) == l {
		return text
	} else if len(text) < l {
		return fmt.Sprintf("%"+string(r)+strconv.Itoa(l)+"s", text)
	}
	return text[:l]
}

func FillRight(text string, l int, r rune) string {
	if len(text) == l {
		return text
	} else if len(text) < l {
		return text + strings.Repeat(string(r), l-len(text))
	}
	return text[:l]
}

func FillLeftBytes(bytes []byte, l int, r rune) []byte {
	return []byte(FillLeft(string(bytes), l, r))
}

func FillLeftZero(text string, l int) string {
	return FillLeft(text, l, '0')
}

func FillLeftBytesZero(bytes []byte, l int) []byte {
	return []byte(FillLeftZero(string(bytes), l))
}

func FillRightZero(text string, l int) string {
	return FillRight(text, l, '0')
}

func FillRightBytes(bytes []byte, l int, r rune) []byte {
	return []byte(FillRight(string(bytes), l, r))
}

func FillRightBytesZero(bytes []byte, l int) []byte {
	return []byte(FillRight(string(bytes), l, '0'))
}

func Repeat(s string, count int) string {
	if count > 0 {
		if count > 0 && len(s)*count/count != len(s) {
			panic("strings: Repeat count causes overflow")
		}

		b := make([]byte, len(s)*count)
		bp := copy(b, s)
		for bp < len(b) {
			copy(b[bp:], b[:bp])
			bp *= 2
		}
		return string(b)
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
