package lygo_strings

import (
	"fmt"
	"testing"
)

func TestFormat(t *testing.T) {
	type args struct {
		s      string
		params []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"simple", args{"text %s %s", []interface{}{"1", 2}}, "text 1 2"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Format(tt.args.s, tt.args.params...); got != tt.want {
				t.Errorf("Format() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrim(t *testing.T) {
	type args struct {
		slice   []string
		trimVal string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestTrimSpaces(t *testing.T) {
	type args struct {
		slice []string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestCapitalizeFirst(t *testing.T) {
	response := CapitalizeFirst("lower words")
	if response != "Lower words" {
		t.Error("Failed")
		t.FailNow()
	}

	response = CapitalizeFirst("lower")
	if response != "Lower" {
		t.Error("Failed")
		t.FailNow()
	}

	response = CapitalizeFirst("")
	if response != "" {
		t.Error("Failed")
		t.FailNow()
	}
}

func TestFill(t *testing.T) {
	s := FillLeft("123", 10, '0')
	fmt.Println(s)

	s = FillLeft("1234567890123", 10, '-')
	fmt.Println(s)

	s = FillLeft("123456789", 10, '0')
	fmt.Println(s)

	s = FillRight("123", 10, '*')
	fmt.Println(s)
}

func TestClear(t *testing.T) {
	text := "this is \n\r    a text é'? \nmultiline\n\n\n\n\n\n\n\ntext"
	s := Clear(text)
	fmt.Println(s)
}

func TestPaginate(t *testing.T) {
	text := "this is \n\r    a text é'? \nmultiline\n\n\n\n\n\n\n\ntext"
	s := Paginate(text)
	fmt.Println(s)

	text = "this is \n\r    a text é'? \nmultiline.\n\n\n\n\n\n\n\ntext"
	s = Paginate(text)
	fmt.Println(s)

	text = "some text here  \n\n�\n\n�\n\n l'adriatica? Grazie "
	s = Paginate(text)
	fmt.Println(s)
}

func TestRemoveDuplicateSpaces(t *testing.T) {
	text := "this is \n\r    a text é'? \nmultiline\n\n\n\n\n\n\n\ntext"
	s := RemoveDuplicateSpaces(text)
	fmt.Println(s)

	text = "this is \n\r    a text é'? \nmultiline.\n\n\n\n\n\n\n\ntext"
	s = RemoveDuplicateSpaces(text)
	fmt.Println(s)

	text = "some text here  \n\n�\n\n�\n\n l'adriatica? Grazie "
	s = RemoveDuplicateSpaces(text)
	fmt.Println(s)
}


func TestSlugify(t *testing.T) {
	text := "this is \n\r    a text  \nmultiline\ntext"
	s := Slugify(text)
	fmt.Println(s)
}

func TestSub(t *testing.T) {
	text := "https://www.facebook.com/Tin-Bòta-1504132526533556/?ref=py_c"
	sub := Sub(text, 0, len(text))
	fmt.Println(sub)
}

func TestSplitLast(t *testing.T) {

	tokens := SplitLast("1.2.3.4", '.')
	fmt.Println(tokens)

	tokens = SplitLast("1.2", '.')
	fmt.Println(tokens)

	tokens = SplitLast("1", '.')
	fmt.Println(tokens)
}

func TestSplitQuoted(t *testing.T) {
	testCases := []struct {
		input string
		want  []string
	}{
		// All tests split on ; and treat " as quoting character.
		{
			input: ``,
			want:  []string{``},
		},
		{
			input: `;`,
			want:  []string{``, ``},
		},
		{
			input: `"`,
			want:  []string{`"`},
		},
		{
			input: `a;b`,
			want:  []string{`a`, `b`},
		},
		{
			input: `a;b;`,
			want:  []string{`a`, `b`, ``},
		},
		{
			input: `a;b;c`,
			want:  []string{`a`, `b`, `c`},
		},
		{
			// Separators are ignored within quoted-runs.
			input: `a;"b;c";d`,
			want:  []string{`a`, `"b;c"`, `d`},
		},
		{
			// Unterminated quoted-run will cause quotes to be ignored from the start of the string.
			input: `"a;b;c;d`,
			want:  []string{`"a`, `b`, `c`, `d`},
		},
		{
			// Unterminated quoted-run will cause quotes to be ignored from the start of the string.
			input: `"a;b";"c;d`,
			want:  []string{`"a`, `b"`, `"c`, `d`},
		},
		{
			// Quotes must be escaped via RFC2047 encoding, not just a backslash.
			// b through c below must not be treated as a single quoted-run.
			input: `a;"b\";\"c";d`,
			want:  []string{`a`, `"b\"`, `\"c"`, `d`},
		},
		{
			input: `a;b\;c`,
			want:  []string{`a`, `b\`, `c`},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			got := SplitQuoted(tc.input, ';', '"')

			t.Logf("\ngot : %q\nwant: %q\n", got, tc.want)

			if len(got) != len(tc.want) {
				t.Errorf("got len %v, want len %v", len(got), len(tc.want))
				return
			}

			for i, g := range got {
				if g != tc.want[i] {
					t.Errorf("Element %v differs", i)
				}
			}
		})
	}
}
