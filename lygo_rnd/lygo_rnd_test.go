package lygo_rnd

import (
	"fmt"
	"testing"
)

func TestAll(t *testing.T) {

	guid := Uuid()
	fmt.Println("Uuid", guid)

	uuid := RndId()
	fmt.Println("RndId", uuid)

	uuid_t := UuidTimestamp()
	fmt.Println("UuidTimestamp", uuid_t)

	rndDigits := RndDigits(6)
	if len(rndDigits)!=6{
		t.Error("Expected 6 digits, got " + rndDigits)
		t.FailNow()
	}
	fmt.Println("RndDigits", rndDigits)

	rndChars := RndChars(6)
	if len(rndChars)!=6{
		t.Error("Expected 6 chars, got " + rndChars)
		t.FailNow()
	}
	fmt.Println("RndChars", rndChars)


}
