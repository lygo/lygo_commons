package test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"fmt"
	"testing"
)

type Item struct {
	Name string `json:"name"`
}

func TestStruct(t *testing.T) {
	var item Item
	err := lygo_json.ReadFromFile("./item.json", &item)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	name := item.Name
	fmt.Println(name)
	// assert.EqualValues(t, "Angelo", name, "Unexpected value")

	var a []Item
	err = lygo_json.ReadFromFile("./array.json", &a)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	name = a[0].Name
	// assert.EqualValues(t, "Angelo", name, "Unexpected value")

	s := lygo_json.Stringify(a)
	fmt.Println(s)
}

func TestRead(t *testing.T) {
	bytes, err := lygo_io.ReadBytesFromFile("./item.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	var m map[string]interface{}
	err = lygo_json.Read(bytes, &m)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	name := m["name"].(string)
	if name != "Angelo" {
		t.Error("Expected name==Angelo")
		t.FailNow()
	}
	fmt.Println(name)
	// assert.EqualValues(t, "Angelo", name, "Unexpected value")

	s := lygo_json.Stringify(m)
	fmt.Println(s)
}

func TestMap(t *testing.T) {
	m, err := lygo_json.ReadMapFromFile("./item.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	name := m["name"]
	fmt.Println(name)
	// assert.EqualValues(t, "Angelo", name, "Unexpected value")

	s := lygo_json.Stringify(m)
	fmt.Println(s)
}

func TestArray(t *testing.T) {
	a, err := lygo_json.ReadArrayFromFile("./array.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	name := a[0]["name"]
	fmt.Println(name)
	// assert.EqualValues(t, "Angelo", name, "Unexpected value")

	s := lygo_json.Stringify(a)
	fmt.Println(s)
}

func TestTryArray(t *testing.T) {
	if v, b := lygo_json.StringToArray("[1,2,3, \"hello\"]"); b {
		fmt.Println(v)
	} else {
		t.Error("Unable to parse")
	}
	if v, b := lygo_json.StringToArray("AAA"); !b {
		fmt.Println("AAA", "is not a JSON object")
	} else {
		t.Error("Should not parse this:", v)
	}
}

func TestTryObject(t *testing.T) {
	if v, b := lygo_json.StringToMap("{\"name\":\"mario\"}"); b {
		fmt.Println(v)
	} else {
		t.Error("Unable to parse")
	}
	if v, b := lygo_json.StringToMap("AAA"); !b {
		fmt.Println("AAA", "is not a JSON object")
	} else {
		t.Error("Should not parse this:", v)
	}
}
