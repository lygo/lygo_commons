package lygo_mime

import (
	"bitbucket.org/lygo/lygo_commons/lygo_csv"
	"strings"
)

var csvData []map[string]string

const (
	fldName        = "Name"
	fldMIMEType    = "MIMEType"
	fldExtension   = "Extension"
	fldDescription = "Description"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func GetMimeTypeByExtension(ext string) string {
	if !strings.HasPrefix(ext, ".") {
		ext = "." + ext
	}
	for _, m := range csvData {
		if strings.ToLower(ext) == m[fldExtension] {
			return m[fldMIMEType]
		}
	}
	return "application/octet-stream"
}

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	options := lygo_csv.NewCsvOptionsDefaults()
	options.FirstRowHeader = true
	options.Comma = ","
	data, err := lygo_csv.ReadAll(mimeCsv, options)
	if nil == err {
		csvData = data
	}
}
