package lygo_mime_test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_mime"
	"fmt"
	"testing"
)

func TestGetMimeTypeByExtension(t *testing.T) {
	expected := "text/plain"
	mime := lygo_mime.GetMimeTypeByExtension("txt")
	if mime!=expected{
		t.Error(fmt.Sprintf("Expected '%s' but got '%s'", expected, mime))
		t.FailNow()
	}
}
