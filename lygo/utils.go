package lygo

import (
	"reflect"
)

//----------------------------------------------------------------------------------------------------------------------
//	h e l p e r s
//----------------------------------------------------------------------------------------------------------------------

func ValueOf(item interface{}) reflect.Value {
	v := reflect.ValueOf(item)
	k := v.Kind()
	switch k {
	case reflect.Ptr:
		return ValueOf(v.Elem().Interface())
	}
	return v
}

func InterfaceOf(item interface{}) interface{} {
	v := ValueOf(item)
	return v.Interface()
}




