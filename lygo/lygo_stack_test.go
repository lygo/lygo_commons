package lygo

import (
	"fmt"
	"testing"
)

func TestStack(t *testing.T) {

	q := NewStack(1)
	for i:=0;i<100;i++{
		q.Push(fmt.Sprintf("item %v", i))
	}
	for i:=0;i<100;i++{
		fmt.Println(q.Pop())
	}
	for i:=0;i<20;i++{
		q.Push(fmt.Sprintf("item %v", i))
	}
	for i:=0;i<100;i++{
		fmt.Println(q.Pop())
	}
}
