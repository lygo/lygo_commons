package lygo

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"fmt"
	"testing"
)

func TestIsMap(t *testing.T) {
	m1 := map[string]interface{}{
		"f1": 1,
		"f2": "hello",
		"ff": map[string]interface{}{
			"a": 1234,
		},
	}
	v, b := Maps.isMap(m1)
	if !b {
		t.Error("expected", m1, "got", v)
		t.FailNow()
	}
}

func TestMerge(t *testing.T) {
	m1 := map[string]interface{}{
		"f1": 1,
		"f2": "hello",
		"ff": map[string]interface{}{
			"a": 1234,
			"c": "c",
			"d":[]string{"1", "2"},
		},
	}
	m2 := map[string]interface{}{
		"f1": 34,
		"f3": 3,
		"ff": map[string]interface{}{
			"b": 567,
			"c": "CCCCCC",
			"d":[]string{"3", "4"},
		},
	}

	m3 := Maps.Merge(true, Maps.Clone(m1), m2)
	if m3["f1"] != 34 {
		t.Error("expected", 34, "got", m3["f1"])
		t.FailNow()
	}
	if m3["f2"] != "hello" {
		t.Error("expected", "hello", "got", m3["f2"])
		t.FailNow()
	}
	if m3["f3"] != 3 {
		t.Error("expected", 3, "got", m3["f3"])
		t.FailNow()
	}
	ff := m3["ff"].(map[string]interface{})
	if ff["a"] != 1234 {
		t.Error("expected", 1234, "got", ff["a"])
		t.FailNow()
	}
	if ff["b"] != 567 {
		t.Error("expected", 567, "got", ff["b"])
		t.FailNow()
	}
	if ff["c"] != "CCCCCC" {
		t.Error("expected", "CCCCCC", "got", ff["c"])
		t.FailNow()
	}
	fmt.Println("OVERWRITE", m3)

	m3 = Maps.Merge(false, Maps.Clone(m1), m2)
	fmt.Println("NO OVERWRITE", m3)
}

func TestGet(t *testing.T) {
	item := loadItem()
	if nil != item {
		v := Maps.Get(item, "synapse.connections.target")
		if nil == v {
			t.Error("property not found")
			t.FailNow()
		}
		fmt.Println("synapse.connections.target", v)

		v = Maps.Get(item, "synapse")
		if nil == v {
			t.Error("property not found")
			t.FailNow()
		}
		fmt.Println("synapse", v)

		v = Maps.Get(item, "doc-size")
		if nil == v {
			t.Error("property not found")
			t.FailNow()
		}
		fmt.Println("doc-size", v)

	} else {
		t.Error("missing item to load")
	}
}

func TestSet(t *testing.T) {
	item := loadItem()
	Maps.Set(item, "synapse.connections.target", 123456)
	v := Maps.Get(item, "synapse.connections.target")
	if v != 123456 {
		t.Error("Failed to assign value", v)
		t.FailNow()
	}
	fmt.Println(v)
}

func loadItem() map[string]interface{} {
	text, err := lygo_io.ReadTextFromFile("./maps_item.json")
	if nil == err {
		var m map[string]interface{}
		err = lygo_json.Read(text, &m)
		if nil == err {
			return m
		}
	}
	return nil
}
