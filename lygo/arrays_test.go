package lygo

import (
	"fmt"
	"testing"
	"time"
)

type MyDoc struct {
	Name string
	Date string
}

func (instance *MyDoc) String() string {
	return instance.Name
}

func TestShuffle(t *testing.T) {
	array := []string{"1", "2", "3", "4", "5"}
	Arrays.Shuffle(array)
	fmt.Println(array)
}

func TestSub(t *testing.T) {
	array := []string{"1", "2", "3", "4", "5"}
	n := Arrays.Sub(array, 1, 1)
	fmt.Println(array, n)
}

func TestIndexOf(t *testing.T) {
	array := []string{"1", "2", "3", "4", "5"}
	i := Arrays.IndexOf("2", array)
	fmt.Println(array, i)
	i = Arrays.IndexOf("2", &array)
	fmt.Println(array, i)
}

func TestSort(t *testing.T) {
	array := []string{"7", "2", "4", "5", "3", "1"}
	Arrays.Sort(array)
	fmt.Println("SORTED ARRAY", array)
	Arrays.Reverse(array)
	fmt.Println("REVERTED ARRAY", array)

	intArr := make([]int, 0)
	for i := 0; i < 1000000; i++ {
		intArr = append(intArr, i)
	}
	// reverse
	now := time.Now()
	Arrays.Reverse(intArr)
	diff := time.Now().Sub(now)
	fmt.Println("Reverse elapsed", diff.Milliseconds(), intArr[0:20])

	// sort
	now = time.Now()
	Arrays.Sort(intArr)
	diff = time.Now().Sub(now)
	fmt.Println("Sort elapsed", diff.Milliseconds(), intArr[0:20])

	// shuffle
	now = time.Now()
	Arrays.Shuffle(intArr)
	diff = time.Now().Sub(now)
	fmt.Println("Shuffle elapsed", diff.Milliseconds(), intArr[0:20])

	// sort desc
	now = time.Now()
	Arrays.SortDesc(intArr)
	diff = time.Now().Sub(now)
	fmt.Println("Sort desc", diff.Milliseconds(), intArr[0:20])
}

func TestAppendUnique(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	array2 := []string{"1", "4", "6", "7"}
	arrayX := Arrays.AppendUnique(&array1, array2).([]interface{})
	if len(arrayX) != 8 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)

	arrayX = Arrays.AppendUnique(arrayX, "hello").([]interface{})
	arrayX = Arrays.AppendUnique(arrayX, "1").([]interface{})
	fmt.Println("NEW ARRAY", arrayX)
}

func TestRemoveIndex(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	arrayX := Arrays.RemoveIndex(3, array1).([]interface{})
	if len(arrayX) != 5 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)
}

func TestRemove(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	arrayX := Arrays.Remove("4", array1).([]interface{})
	if len(arrayX) != 5 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)
}

func TestReplaceAll(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", "4"}
	arrayX := Arrays.ReplaceAll("4", "4-replaced", array1).([]interface{})
	if len(arrayX) != 6 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)
}

func TestReplace(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", "4", "4"}
	arrayX := Arrays.Replace("4", "4-replaced", array1, 2, false).([]interface{})
	if len(arrayX) != 7 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println(array1, "->", arrayX)

	array1 = []interface{}{"old_person1", "old_person2", "person3", "person4", "old_person2", "person5", "person6"}
	arrayX = Arrays.Replace([]string{"old_person1", "old_person2"}, "new_person", array1, 2, true).([]interface{})
	if len(arrayX) != 6 {
		t.Error("Invalid number of items", len(arrayX))
		t.FailNow()
	}
	fmt.Println("REPLACE 2: ", array1, "->", arrayX)

	array1 = []interface{}{"old_person1", "old_person2", "person3", "person4", "old_person2", "person5", "person6"}
	arrayX = Arrays.Replace([]string{"old_person1", "old_person2"}, "new_person", array1, -1, true).([]interface{})
	if len(arrayX) != 5 {
		t.Error("Invalid number of items", len(arrayX))
		t.FailNow()
	}
	fmt.Println("REPLACE ALL: ", array1, "->", arrayX)
}

func TestAppendUniqueFunc(t *testing.T) {
	doc1 := &MyDoc{
		Name: "Mario",
		Date: "now",
	}
	doc2 := &MyDoc{
		Name: "Mario",
		Date: "now",
	}
	doc3 := &MyDoc{
		Name: "Ivan",
		Date: "now",
	}
	slice1 := []*MyDoc{doc1}
	slice2 := []*MyDoc{doc1, doc2, doc3}

	slice1 = Arrays.AppendUniqueFunc(slice1, slice2, func(t, s interface{}) bool {
		n1 := t.(*MyDoc).Name
		n2 := s.(*MyDoc).Name
		return n1 != n2
	}).([]*MyDoc)
	fmt.Println(len(slice1), slice1)

	doc4 := &MyDoc{
		Name: "Podovsky",
		Date: "now",
	}
	slice1 = Arrays.AppendUniqueFunc(slice1, doc4, func(t, s interface{}) bool {
		n1 := t.(*MyDoc).Name
		n2 := s.(*MyDoc).Name
		return n1 != n2
	}).([]*MyDoc)
	fmt.Println(len(slice1), slice1)
}

func TestFilter(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	arrayX := Arrays.Filter(array1, func(item interface{}) bool {
		return item == "1"
	}).([]interface{})
	if len(arrayX) != 1 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)
}

func TestMap(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	arrayX := Arrays.Map(array1, func(item interface{}) interface{} {
		return fmt.Sprintf("%v-mapped", item)
	}).([]interface{})
	if len(arrayX) != len(array1) {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("NEW ARRAY", arrayX)
}

func TestIndexOfFunc(t *testing.T) {
	array1 := []interface{}{"1", "2", "3", "4", "5", 1}
	idx := Arrays.IndexOfFunc(array1, func(item interface{}) bool {
		return item == "2"
	})
	if idx != 1 {
		t.Error("Invalid number of items")
		t.FailNow()
	}
	fmt.Println("INDEX", idx)
}