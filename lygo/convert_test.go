package lygo

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

type MyStruct struct {
	Field1 string `json:"field1"`
	Field2 string `json:"field2"`
}

func TestToString(t *testing.T) {
	type args struct {
		val interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"nil", args{nil}, ""},
		{"string", args{"hola"}, "hola"},
		{"int", args{123}, "123"},
		{"float32", args{123.456}, "123.456"},
		{"boolean", args{true}, "true"},
		{"array", args{[]int{1, 2, 3}}, "[1,2,3]"},
		{"[]byte", args{[]byte("hello")}, "hello"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Convert.ToString(tt.args.val); got != tt.want {
				t.Errorf("ToString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToInt(t *testing.T) {
	var v interface{} = 12345678.456
	fmt.Println(fmt.Sprintf("%v", Convert.ToInt(v)))
}

func TestToArrayOfString(t *testing.T) {
	arr := Convert.ToArrayOfString([]string{"1", "2"})
	fmt.Println(arr)
	// test.ASSERT_EQ([]string{"1", "2"}, arr, t)

	arr = Convert.ToArrayOfString([]int{1, 2})
	fmt.Println(arr)
	// test.ASSERT_EQ([]string{"1", "2"}, arr, t)

	arr = Convert.ToArrayOfString([]interface{}{1, 2})
	fmt.Println(arr)
	// test.ASSERT_EQ([]string{"1", "2"}, arr, t)
}

func TestToArrayOfInt(t *testing.T) {
	// serialize and desirialize
	packet := []int8{126, 126, 126, 126, 126, 126, 126, 126, 126, 85, 13, 0, 0, 1, 1, 4, 2, 60, -21, 106, 48, 4, 2, 80, -21, -126, 78, 4, 2, 77, -21, 113, 112, 4, 2, 67, -21, 109, -18, 4, 2, 92, -21, 113, 74, 4, 2, 80, -21, 120, -92, 4, 2, 93, -21, -117, -91, 4, 2, 73, -21, 119, -98, 4, 2, 69, -21, 104, -51, 4, 2, 70, -21, 112, -95, 4, 2, 54, -21, 114, -98, 4, 2, 90, -21, -121, -57, 4, 2, -128, -21, 117, -111, 4, 2, 90, -21, 106, -65, 4, 2, 37, -21, 114, 43, 4, 2, 58, -21, 124, 75, 4, 2, 108, -21, -113, -71, 4, 2, 110, -21, -116, 1, 4, 2, 20, -21, -120, 38, 4, 2, 33, -21, -101, -65, 4, 2, 16, -21, -87, -1, 4, 2, 25, -21, -104, -123, 4, 2, 20, -21, 87, -34, 4, 1, -3, -21, 25, -71, 4, 1, -10, -21, 9, 48, 4, 1, -22, -22, -15, 89, 4, 1, -22, -21, 8, 79, 4, 2, 1, -21, 4, -47, 4, 1, -12, -21, 22, -104, 4, 1, -10, -21, 79, 103, 4, 1, -42, -21, 104, 55, 4, 2, 28, -21, -127, 119, 4, 2, 42, -21, 111, -60, 4, 2, 72, -21, 97, 92, 4, 2, 40, -21, 103, -115, 4, 2, 25, -21, 114, -26, 4, 2, 25, -21, -126, 117, 4, 2, 34, -21, 118, -72, 4, 1, -25, -21, 108, -120, 4, 1, -33, -21, -122, -100, 4, 1, -23, -21, -107, 62, 4, 2, 20, -21, -127, -101, 4, 1, -22, -21, 124, -64, 4, 1, -55, -21, 119, -113, 4, 1, -32, -21, -127, 74, 4, 1, -33, -21, -113, 37, 4, 2, 21, -21, -93, 77, 4, 1, -40, -21, -124, -43, 4, 1, -114, -21, 118, 89, 4, 1, -59, -21, -122, -55, 4, 1, -87, -21, -120, -13, 4, 1, -75, -21, -111, -62, 4, 1, -77, -21, 123, 127, 4, 1, -52, -21, 114, -26, 4, 1, -49, -21, -124, -89, 4, 1, -50, -21, -125, 74, 4, 1, -45, -21, -116, 118, 4, 1, -41, -21, 125, -111, 4, 1, 108, -21, 110, -102, 4, 1, 120, -21, 122, -28, 4, 1, -108, -21, -119, -60, 4, 1, -56, -21, -111, 29, 4, 1, -35, -21, -123, -117, 127, 127, 127, 127, 127, 127, 127, 127, 127}
	body := map[string]interface{}{
		"packet": packet,
	}
	data, err := json.Marshal(&body)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	var m map[string]interface{}
	err = json.Unmarshal(data, &m)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	value := Convert.ToArrayOfInt(m["packet"])
	if len(value) != len(packet) {
		t.Error("Invalid packet length")
		t.FailNow()
	}

	value64 := Convert.ToArrayOfInt64(m["packet"])
	if len(value64) != len(packet) {
		t.Error("Invalid packet length")
		t.FailNow()
	}

}

func TestToInt64(t *testing.T) {
	value := Convert.ToInt64("1234")
	if value != 1234 {
		t.Error("Expected 1234")
		t.FailNow()
	}
}

func TestToMap(t *testing.T) {
	text := "foo=1234&boo=dhjdhdh"
	m := Convert.ToMap(text)
	if m["foo"] != "1234" {
		t.Error("Expected '1234'")
		t.FailNow()
	}
	fmt.Println(m)

	ms := Convert.ToMapOfString(text)
	if ms["boo"] != "dhjdhdh" {
		t.Error("Expected '1234'")
		t.FailNow()
	}
	fmt.Println(ms)

	text = "--------------------/n/tfoo1234" // invalid
	m = Convert.ToMap(text)
	if nil != m {
		t.Error("Expected nil")
		t.FailNow()
	}

	data := []uint8("{\"data\":1234}")
	m = Convert.ToMap(data)
	fmt.Println(m)

	str := &MyStruct{
		Field1: "ABC",
		Field2: "DEF",
	}
	m = Convert.ToMap(str)
	fmt.Println(m)
}

func TestForceMap(t *testing.T) {
	text := "foo=1234&boo=dhjdhdh"
	m := Convert.ForceMap(text)
	if m["foo"] != "1234" {
		t.Error("Expected '1234'")
		t.FailNow()
	}
	if m["boo"] != "dhjdhdh" {
		t.Error("Expected 'dhjdhdh'")
		t.FailNow()
	}
	fmt.Println(m)

	data := []uint8("{\"data\":1234}")
	m = Convert.ForceMap(data)
	fmt.Println(m)

	m = Convert.ForceMap("['aaaa','bbbb']")
	if m["0"] != "aaaa" {
		t.Error("Expected 'aaaa'")
		t.FailNow()
	}
	fmt.Println(m)
}

func TestToDuration(t *testing.T) {
	s := "second:3"
	d := Convert.ToDuration(s)
	if d != 3*time.Second {
		t.Error(fmt.Sprintf("Expected '%v' got '%v'", 3*time.Second, d))
		t.FailNow()
	}
	fmt.Println(s, d)

	d = Convert.ToDuration(3000)
	if d != 3000*time.Millisecond {
		t.Error(fmt.Sprintf("Expected '%v' got '%v'", 3000*time.Millisecond, d))
		t.FailNow()
	}
	fmt.Println(3000, d)
}
