package lygo

import "strings"

type MapsHelper struct {
}

var Maps *MapsHelper

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	Maps = new(MapsHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *MapsHelper) Clone(m map[string]interface{}) map[string]interface{} {
	response := make(map[string]interface{})
	for k, v := range m {
		tm, tmb := instance.isMap(v)
		if tmb {
			response[k] = instance.Clone(tm)
		} else {
			response[k] = v
		}
	}
	return response
}

func (instance *MapsHelper) Merge(overwrite bool, target map[string]interface{}, sources ...map[string]interface{}) map[string]interface{} {
	if nil != target {
		for _, source := range sources {
			instance.merge(target, source, overwrite, nil)
		}
	}
	return target
}

func (instance *MapsHelper) MergeExclusion(overwrite bool, exclusions []string, target map[string]interface{}, sources ...map[string]interface{}) map[string]interface{} {
	if nil != target {
		for _, source := range sources {
			instance.merge(target, source, overwrite, exclusions)
		}
	}
	return target
}

func (instance *MapsHelper) Get(m map[string]interface{}, path string) interface{} {
	if nil != m && len(path) > 0 {
		itemMap := m
		tokens := strings.Split(path, ".")
		length := len(tokens)
		for i := 0; i < length; i++ {
			token := tokens[i]
			tv, tb := itemMap[token]
			if tb {
				if i == length-1 {
					return tv
				} else {
					if mm, b := instance.isMap(tv); b {
						itemMap = mm
					}
				}
			}
		}
	}
	return nil
}

func (instance *MapsHelper) Set(m map[string]interface{}, path string, value interface{}) {
	if nil != m && len(path) > 0 {
		itemMap := m
		tokens := strings.Split(path, ".")
		length := len(tokens)
		for i := 0; i < length; i++ {
			token := tokens[i]
			if i == length-1 {
				itemMap[token] = value
			} else {
				if _, tb := itemMap[token]; !tb {
					itemMap[token] = map[string]interface{}{}
				}
				itemMap = itemMap[token].(map[string]interface{})
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *MapsHelper) isMap(v interface{}) (map[string]interface{}, bool) {
	if m, b := v.(map[string]interface{}); b {
		return m, true
	}
	return nil, false
}

func (instance *MapsHelper) merge(target map[string]interface{}, source map[string]interface{}, overwrite bool, exclusion []string) {
	if nil != target && nil != source {
		for sk, sv := range source {
			if len(exclusion) == 0 || Arrays.IndexOf(sk, exclusion) < 0 {
				tv, tb := target[sk]
				tm, tmb := instance.isMap(tv)
				sm, smb := instance.isMap(sv)
				if smb && tmb {
					instance.merge(tm, sm, overwrite, exclusion)
					continue
				} else if b, _ := Compare.IsArray(tv); b && !overwrite {
					target[sk] = Arrays.AppendUnique(tv, sv)
					continue
				}
				if !tb || overwrite {
					target[sk] = sv
					continue
				}
			}
		}
	}
}
