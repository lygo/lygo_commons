package lygo_fmt

import (
	"fmt"
	"testing"
	"time"
)

func TestDate(t *testing.T) {
	now := time.Now()

	patterns := []string{
		"yyyy-MM-dd",
		"yyyy-MM-dd HH:mm:ss",
		"yyyy-MM-dd HH:mm:ssZ",
	}

	for _, pattern := range patterns {
		f := FormatDate(now, pattern)
		fmt.Println(f)
		d, err := ParseDate(f, pattern)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		fmt.Println(d)
	}

}

func TestPrintMap(t *testing.T) {
	m := map[string]interface{}{
		"field1":     "1234",
		"fieldArray": []string{"a", "b", "d"},
		"fieldMap": map[string]interface{}{
			"a": "hello",
			"b": "world",
			"c":map[string]interface{}{
				"aa": "-hello",
				"bb": "-world",
			},
		},
		"field2":"Hello map",
	}
	fmt.Println(FormatMap(m))
}

func TestPrintNumber(t *testing.T) {
	res := FormatInteger(1234577890.45, "")
	exp := "1,234,577,890"
	if res!=exp{
		t.Error(fmt.Sprintf("Expected '%s', bit got '%s'", exp, res))
	}
	fmt.Println(res)
	res = FormatFloat(1234577890.45, "")
	exp = "1,234,577,890.45"
	if res!=exp{
		t.Error(fmt.Sprintf("Expected '%s', bit got '%s'", exp, res))
	}
	fmt.Println(res)
}
