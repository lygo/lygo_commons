package lygo_fmt

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_mu"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bytes"
	"fmt"
	templateHtml "html/template"
	"math"
	"strconv"
	"strings"
	templateText "text/template"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	T E M P L A T E
//----------------------------------------------------------------------------------------------------------------------

func MergeText(text string, data interface{}) (string, error) {
	if nil == data {
		data = struct{}{}
	}
	buff := bytes.NewBufferString("")
	t := templateText.Must(templateText.New("template").Parse(strings.Trim(text, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}

func MergeHtml(html string, data interface{}) (string, error) {
	if nil == data {
		data = struct{}{}
	}
	buff := bytes.NewBufferString("")
	t := templateHtml.Must(templateHtml.New("template").Parse(strings.Trim(html, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}

//----------------------------------------------------------------------------------------------------------------------
//	D A T E
//----------------------------------------------------------------------------------------------------------------------

func FormatDate(dt time.Time, pattern string) string {
	return lygo_date.FormatDate(dt, pattern)
}

func ParseDate(dt string, pattern string) (time.Time, error) {
	return lygo_date.ParseDate(dt, pattern)
}

//----------------------------------------------------------------------------------------------------------------------
//	M A P
//----------------------------------------------------------------------------------------------------------------------

func FormatMap(i interface{}) string {
	m := lygo_conv.ToMap(i)
	if nil != m {
		return printMap(m, 0)
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	B Y T E S
//----------------------------------------------------------------------------------------------------------------------

func FormatBytes(i interface{}) string {
	n := uint64(lygo_conv.ToInt64(i))
	return lygo_mu.FmtBytes(n)
}

//----------------------------------------------------------------------------------------------------------------------
//	N U M B E R S
//----------------------------------------------------------------------------------------------------------------------

var renderFloatPrecisionMultipliers = [10]float64{
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000,
	10000000,
	100000000,
	1000000000,
}

var renderFloatPrecisionRounders = [10]float64{
	0.5,
	0.05,
	0.005,
	0.0005,
	0.00005,
	0.000005,
	0.0000005,
	0.00000005,
	0.000000005,
	0.0000000005,
}

func FormatInteger(i interface{}, pattern string) string {
	if len(pattern) == 0 {
		pattern = "#,###."
	}
	n := lygo_conv.ToInt64(i)
	return renderFloat(pattern, float64(n))
}

func FormatFloat(i interface{}, pattern string) string {
	if len(pattern) == 0 {
		pattern = "#,###.##"
	}
	n := lygo_conv.ToFloat64(i)
	return renderFloat(pattern, n)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func printMap(m map[string]interface{}, level int) string {
	var buf bytes.Buffer
	prefix := ""
	if level > 0 {
		prefix = lygo_strings.FillLeft("", level, ' ')
	}
	prefix = strings.ReplaceAll(prefix, " ", "\t")
	for k, v := range m {
		if mm, b := v.(map[string]interface{}); b {
			buf.WriteString(fmt.Sprintf("%v %v:\n", prefix, k))
			buf.WriteString(printMap(mm, level+1))
		} else {
			buf.WriteString(fmt.Sprintf("%v %v: %v\n", prefix, k, v))
		}
	}
	return buf.String()
}

/**
	Examples of format strings, given n = 12345.6789:
	"#,###.##" => "12,345.67"
	"#,###." => "12,345"
	"#,###" => "12345,678"
	"#\u202F###,##" => "12 345,67"
	"#.###,###### => 12.345,678900
	"" (aka default format) => 12,345.67
	The highest precision allowed is 9 digits after the decimal symbol.
**/
func renderFloat(format string, n float64) string {
	// Special cases:
	//   NaN = "NaN"
	//   +Inf = "+Infinity"
	//   -Inf = "-Infinity"
	if math.IsNaN(n) {
		return "NaN"
	}
	if n > math.MaxFloat64 {
		return "Infinity"
	}
	if n < -math.MaxFloat64 {
		return "-Infinity"
	}

	// default format
	precision := 2
	decimalStr := "."
	thousandStr := ","
	positiveStr := ""
	negativeStr := "-"

	if len(format) > 0 {
		// If there is an explicit format directive,
		// then default values are these:
		precision = 9
		thousandStr = ""

		// collect indices of meaningful formatting directives
		formatDirectiveChars := []rune(format)
		formatDirectiveIndices := make([]int, 0)
		for i, char := range formatDirectiveChars {
			if char != '#' && char != '0' {
				formatDirectiveIndices = append(formatDirectiveIndices, i)
			}
		}

		if len(formatDirectiveIndices) > 0 {
			// Directive at index 0:
			//   Must be a '+'
			//   Raise an error if not the case
			// index: 0123456789
			//        +0.000,000
			//        +000,000.0
			//        +0000.00
			//        +0000
			if formatDirectiveIndices[0] == 0 {
				if formatDirectiveChars[formatDirectiveIndices[0]] != '+' {
					panic("renderFloat(): invalid positive sign directive")
				}
				positiveStr = "+"
				formatDirectiveIndices = formatDirectiveIndices[1:]
			}

			// Two directives:
			//   First is thousands separator
			//   Raise an error if not followed by 3-digit
			// 0123456789
			// 0.000,000
			// 000,000.00
			if len(formatDirectiveIndices) == 2 {
				if (formatDirectiveIndices[1] - formatDirectiveIndices[0]) != 4 {
					panic("renderFloat(): thousands separator directive must be followed by 3 digit-specifiers")
				}
				thousandStr = string(formatDirectiveChars[formatDirectiveIndices[0]])
				formatDirectiveIndices = formatDirectiveIndices[1:]
			}

			// One directive:
			//   Directive is decimal separator
			//   The number of digit-specifier following the separator indicates wanted precision
			// 0123456789
			// 0.00
			// 000,0000
			if len(formatDirectiveIndices) == 1 {
				decimalStr = string(formatDirectiveChars[formatDirectiveIndices[0]])
				precision = len(formatDirectiveChars) - formatDirectiveIndices[0] - 1
			}
		}
	}

	// generate sign part
	var signStr string
	if n >= 0.000000001 {
		signStr = positiveStr
	} else if n <= -0.000000001 {
		signStr = negativeStr
		n = -n
	} else {
		signStr = ""
		n = 0.0
	}

	// split number into integer and fractional parts
	intf, fracf := math.Modf(n + renderFloatPrecisionRounders[precision])

	// generate integer part string
	intStr := strconv.Itoa(int(intf))

	// add thousand separator if required
	if len(thousandStr) > 0 {
		for i := len(intStr); i > 3; {
			i -= 3
			intStr = intStr[:i] + thousandStr + intStr[i:]
		}
	}

	// no fractional part, we can leave now
	if precision == 0 {
		return signStr + intStr
	}

	// generate fractional part
	fracStr := strconv.Itoa(int(fracf * renderFloatPrecisionMultipliers[precision]))
	// may need padding
	if len(fracStr) < precision {
		fracStr = "000000000000000"[:precision-len(fracStr)] + fracStr
	}

	return signStr + intStr + decimalStr + fracStr
}
