/**
	THIS CODE HAS BEEN FORKED FROM ORIGINAL REPOSITORY:
	https://github.com/alixaxel/pagerank
 */

package lygo_pagerank

import (
	"math"
)

type node struct {
	weight   float64
	outbound float64
}

//----------------------------------------------------------------------------------------------------------------------
//	Graph
//----------------------------------------------------------------------------------------------------------------------

// Graph holds node and edge data.
type Graph struct {
	edges map[uint32](map[uint32]float64)
	nodes map[uint32]*node
}

// NewGraph initializes and returns a new graph.
func NewGraph() *Graph {
	return &Graph{
		edges: make(map[uint32](map[uint32]float64)),
		nodes: make(map[uint32]*node),
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------


// Link creates a weighted edge between a source-target node pair.
// If the edge already exists, the weight is incremented.
func (instance *Graph) Link(source, target uint32, weight float64) {
	if _, ok := instance.nodes[source]; ok == false {
		instance.nodes[source] = &node{
			weight:   0,
			outbound: 0,
		}
	}

	instance.nodes[source].outbound += weight

	if _, ok := instance.nodes[target]; ok == false {
		instance.nodes[target] = &node{
			weight:   0,
			outbound: 0,
		}
	}

	if _, ok := instance.edges[source]; ok == false {
		instance.edges[source] = map[uint32]float64{}
	}

	instance.edges[source][target] += weight
}

// Rank computes the PageRank of every node in the directed graph.
// α (alpha) is the damping factor, usually set to 0.85.
// ε (epsilon) is the convergence criteria, usually set to a tiny value.
//
// This method will run as many iterations as needed, until the graph converges.
func (instance *Graph) Rank(α, ε float64, callback func(id uint32, rank float64)) {
	Δ := float64(1.0)
	inverse := 1 / float64(len(instance.nodes))

	// Normalize all the edge weights so that their sum amounts to 1.
	for source := range instance.edges {
		if instance.nodes[source].outbound > 0 {
			for target := range instance.edges[source] {
				instance.edges[source][target] /= instance.nodes[source].outbound
			}
		}
	}

	for key := range instance.nodes {
		instance.nodes[key].weight = inverse
	}

	for Δ > ε {
		leak := float64(0)
		nodes := map[uint32]float64{}

		for key, value := range instance.nodes {
			nodes[key] = value.weight

			if value.outbound == 0 {
				leak += value.weight
			}

			instance.nodes[key].weight = 0
		}

		leak *= α

		for source := range instance.nodes {
			for target, weight := range instance.edges[source] {
				instance.nodes[target].weight += α * nodes[source] * weight
			}

			instance.nodes[source].weight += (1-α)*inverse + leak*inverse
		}

		Δ = 0

		for key, value := range instance.nodes {
			Δ += math.Abs(value.weight - nodes[key])
		}
	}

	for key, value := range instance.nodes {
		callback(key, value.weight)
	}
}

// Reset clears all the current graph data.
func (instance *Graph) Reset() {
	instance.edges = make(map[uint32](map[uint32]float64))
	instance.nodes = make(map[uint32]*node)
}
