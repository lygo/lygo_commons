package lygo_fmttable

import (
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"strings"
	"testing"
)

var text = "The quick brown fox jumps over the lazy dog."

func TestWrap(t *testing.T) {
	exp := []string{
		"The", "quick", "brown", "fox",
		"jumps", "over", "the", "lazy", "dog."}

	got, _ := WrapString(text, 6)
	checkEqual(t, len(got), len(exp))
}

func TestWrapOneLine(t *testing.T) {
	exp := "The quick brown fox jumps over the lazy dog."
	words, _ := WrapString(text, 500)
	checkEqual(t, strings.Join(words, string(sp)), exp)

}

func TestUnicode(t *testing.T) {
	input := "Česká řeřicha"
	var wordsUnicode []string
	if lygo_strings.IsEastAsian() {
		wordsUnicode, _ = WrapString(input, 14)
	} else {
		wordsUnicode, _ = WrapString(input, 13)
	}
	// input contains 13 (or 14 for CJK) runes, so it fits on one line.
	checkEqual(t, len(wordsUnicode), 1)
}

func TestDisplayWidth(t *testing.T) {
	input := "Česká řeřicha"
	want := 13
	if lygo_strings.IsEastAsian() {
		want = 14
	}
	if n := DisplayWidth(input); n != want {
		t.Errorf("Wants: %d Got: %d", want, n)
	}
	input = "\033[43;30m" + input + "\033[00m"
	checkEqual(t, DisplayWidth(input), want)
}
