# LyGo Commons #

![](icon.png)

Utility library.

This folder contains some utility packages.

Mose of them are pure Go, only some uses external modules 
but always in pure Go.

No dependencies from other external binaries suing C bindings.

## How to Use

To use just call:

```
go get bitbucket.org/lygo/lygo_commons@v0.1.121
```

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.121
git push origin v0.1.121
```

## Dependencies

`go get -u github.com/google/uuid`
