package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_async"
	"errors"
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
)

const LIMIT = 100

func TestExample(t *testing.T) {
	pool := lygo_async.NewConcurrentPool(LIMIT)
	var ii int32 = 0
	for i := 0; i < 100000; i++ {
		pool.Run(func() error {
			atomic.AddInt32(&ii, 1)
			// do some work
			count := pool.Count()
			if count > LIMIT+1 {
				err := fmt.Sprintf("CONSTRAINT VIOLATION. NUMBER OF THREADS EXCEEDED: %v", count)
				fmt.Println(atomic.LoadInt32(&ii), "CONSTRAINT VIOLATION. NUMBER OF THREADS EXCEEDED: ", err)
				return errors.New(err)
			} else {
				fmt.Println(atomic.LoadInt32(&ii), "Pool Size:", count)
			}
			//var wait time.Duration = time.Duration(lygo_rnd.Between(100, 300)) * time.Millisecond
			//time.Sleep(wait)

			return nil
		})
	}
	err := pool.Wait().ErrorOrNil()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestLimit(t *testing.T) {

	N := 100

	pool := lygo_async.NewConcurrentPool(LIMIT)
	m := map[int]bool{}
	lock := &sync.Mutex{}

	max := int32(0)
	for i := 0; i < N; i++ {
		x := i
		ticket := pool.Run(func() error {
			lock.Lock()
			m[x] = true
			currentMax := pool.Count()
			if currentMax >= max {
				max = currentMax
			}
			lock.Unlock()

			return nil // no error
		})
		if ticket > LIMIT-1 {
			t.Errorf("expected max: %d, got %d", LIMIT, ticket)
		}
	}

	err := pool.Wait().ErrorOrNil()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	t.Log("results:", len(m))
	t.Log("max:", max)

	if len(m) != N {
		t.Error("invalid num of results", len(m))
	}

	if max > int32(LIMIT) || max == 0 {
		t.Error("invalid max", max)
	}
}

func TestErrors(t *testing.T) {
	err1 := errors.New("err_test: 1")
	err2 := errors.New("err_test: 2")

	cases := []struct {
		errs      []error
		nilResult bool
	}{
		{errs: []error{}, nilResult: true},
		{errs: []error{nil}, nilResult: true},
		{errs: []error{err1}},
		{errs: []error{err1, nil}},
		{errs: []error{err1, nil, err2}},
	}

	pool := lygo_async.NewConcurrentPool(2)
	for _, tc := range cases {

		for _, err := range tc.errs {
			err := err
			i:=pool.Run(func() error {
				return err
			})
			fmt.Println("task:", i, err)
		}

		poolErrors := pool.Wait()
		if poolErrors != nil {
			for i := range tc.errs {
				if tc.errs[i] != nil && !strings.Contains(poolErrors.Error(), tc.errs[i].Error()) {
					t.Fatalf("expected error to contain %q, actual: %v", tc.errs[i].Error(), poolErrors)
				}
			}
		} else if !tc.nilResult {
			t.Fatalf("ConcurrentPool.Wait() should not have returned nil for errs: %v", tc.errs)
		}
	}
}
