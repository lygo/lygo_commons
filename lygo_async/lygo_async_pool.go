package lygo_async

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"sync"
	"sync/atomic"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const (
	// DefaultMaxConcurrent is max number of concurrent routines
	DefaultMaxConcurrent = 100
)

type GoRoutineWrapper func() error

type ConcurrentPool struct {
	limit                int
	tickets              chan int
	numOfRunningRoutines int32
	wg                   sync.WaitGroup
	errMux               sync.Mutex
	err                  *lygo_errors.Error
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewConcurrentPool(limit int) *ConcurrentPool {
	if limit < 1 {
		limit = DefaultMaxConcurrent
	}

	// allocate a limiter instance
	instance := new(ConcurrentPool)
	instance.limit = limit
	instance.tickets = make(chan int, limit) // buffered channel with a limit
	instance.numOfRunningRoutines = 0

	// allocate the tickets:
	for i := 0; i < instance.limit; i++ {
		instance.tickets <- i
	}

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ConcurrentPool) Limit() int {
	if nil != instance {
		return instance.limit
	}
	return 0
}

func (instance *ConcurrentPool) Count() int32 {
	if nil != instance {
		return atomic.LoadInt32(&instance.numOfRunningRoutines)
	}
	return 0
}

// Run Execute adds a function to the execution queue.
// If num of go routines allocated by this instance is < limit
// launch a new go routine to execute job
// else wait until a go routine becomes available
func (instance *ConcurrentPool) Run(f GoRoutineWrapper) int {
	if nil != instance {
		// pop a ticket
		ticket := <-instance.tickets
		atomic.AddInt32(&instance.numOfRunningRoutines, 1)

		instance.wg.Add(1)

		go func() {

			defer func() {
				// push a ticket
				instance.tickets <- ticket
				atomic.AddInt32(&instance.numOfRunningRoutines, -1)
			}()

			// run the job
			defer instance.wg.Done()
			if err := f(); err != nil {
				instance.errMux.Lock()
				instance.err = lygo_errors.Append(instance.err, err)
				instance.errMux.Unlock()
			}
		}()

		return ticket
	}
	return -1
}

// Wait all jobs are executed, if any in queue
func (instance *ConcurrentPool) Wait() *lygo_errors.Error {
	if nil != instance {
		instance.wg.Wait()
		instance.errMux.Lock()
		defer instance.errMux.Unlock()
		return instance.err
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
