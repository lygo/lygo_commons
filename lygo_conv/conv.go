package lygo_conv

import (
	"encoding/json"
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// ToDuration @Deprecated: use Convert.
func ToDuration(val interface{}) time.Duration {
	value := int64(1)
	if nil != val {
		if s, b := val.(string); b {
			// TIMELINE
			tl := strings.Split(s, ":") // hour:12
			um := tl[0]
			if len(tl) == 2 {
				value = ToInt64(tl[1])
				if value == 0 {
					value = 1
				}
			}
			switch um {
			case "millisecond":
				return time.Duration(value) * time.Millisecond
			case "second":
				return time.Duration(value) * time.Second
			case "minute":
				return time.Duration(value) * time.Minute
			case "hour":
				return time.Duration(value) * time.Hour
			default:
				return 12 * time.Hour
			}
		} else if i, b := val.(int); b {
			value = int64(i)
		} else if i, b := val.(int8); b {
			value = int64(i)
		} else if i, b := val.(int16); b {
			value = int64(i)
		} else if i, b := val.(int32); b {
			value = int64(i)
		} else if i, b := val.(int64); b {
			value = i
		} else if i, b := val.(uint); b {
			value = int64(i)
		} else if i, b := val.(uint8); b {
			value = int64(i)
		} else if i, b := val.(uint16); b {
			value = int64(i)
		} else if i, b := val.(uint32); b {
			value = int64(i)
		} else if i, b := val.(uint64); b {
			value = int64(i)
		}
	}
	return time.Duration(value) * time.Millisecond
}

func ToArray(val ...interface{}) []interface{} {
	if nil == val {
		return nil
	}
	aa := toArray(val...)
	return aa
}

func ToArrayOfString(val ...interface{}) []string {
	if nil == val {
		return nil
	}
	aa := toArrayOfString(val...)
	return aa
}

func ToArrayOfByte(val interface{}) []byte {
	if nil == val {
		return nil
	}
	return toArrayOfByte(val)
}

func ToArrayOfInt(val interface{}) []int {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfInt(v)
		} else if v, b := val.([]int); b {
			return v
		}
	}
	return nil
}

func ToArrayOfInt8(val interface{}) []int8 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfInt8(v)
		} else if v, b := val.([]int8); b {
			return v
		}
	}
	return nil
}

func ToArrayOfInt16(val interface{}) []int16 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfInt16(v)
		} else if v, b := val.([]int16); b {
			return v
		}
	}
	return nil
}

func ToArrayOfInt32(val interface{}) []int32 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfInt32(v)
		} else if v, b := val.([]int32); b {
			return v
		}
	}
	return nil
}

func ToArrayOfInt64(val interface{}) []int64 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfInt64(v)
		} else if v, b := val.([]int64); b {
			return v
		}
	}
	return nil
}

func ToArrayOfFloat32(val interface{}) []float32 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfFloat32(v)
		} else if v, b := val.([]float32); b {
			return v
		}
	}
	return nil
}

func ToArrayOfFloat64(val interface{}) []float64 {
	if b, _ := IsArray(val); b {
		if v, b := val.([]interface{}); b {
			return toArrayOfFloat64(v)
		} else if v, b := val.([]float64); b {
			return v
		}
	}
	return nil
}

func ToString(val interface{}) string {
	if nil == val {
		return ""
	}
	// string
	s, ss := val.(string)
	if ss {
		return s
	}
	// integer
	i, ii := val.(int)
	if ii {
		return strconv.Itoa(i)
	}
	// float32
	f, ff := val.(float32)
	if ff {
		return fmt.Sprintf("%g", f) // Exponent as needed, necessary digits only
	}
	// float 64
	F, FF := val.(float64)
	if FF {
		return fmt.Sprintf("%g", F) // Exponent as needed, necessary digits only
		// return strconv.FormatFloat(F, 'E', -1, 64)
	}

	// boolean
	b, bb := val.(bool)
	if bb {
		return strconv.FormatBool(b)
	}

	// array
	if aa, _ := IsArray(val); aa {
		// byte array??
		if ba, b := val.([]byte); b {
			return string(ba)
		} else {
			data, err := json.Marshal(val)
			if nil == err {
				return string(data)
			}
			/*
				response := []string{}
				// array := make([]interface{}, tt.Len())
				for i := 0; i < tt.Len(); i++ {
					v := tt.Index(i).Interface()
					s := ToString(v)
					response = append(response, s)
				}
				return "[" + strings.Wait(response, ",") + "]"
			*/
		}
	}

	// map
	if b, _ := IsMap(val); b {
		data, err := json.Marshal(val)
		if nil == err {
			return string(data)
		}
	}

	// struct
	if b, _ := IsStruct(val); b {
		data, err := json.Marshal(val)
		if nil == err {
			return string(data)
		}
	}

	// undefined value
	return fmt.Sprintf("%v", val)
}

func ToStringQuoted(val interface{}) string {
	response := ToString(val)
	if _, b := val.(string); b {
		if strings.Index(response, "\"") == -1 {
			response = strconv.Quote(response)
		}
	}
	return response
}

func Int8ToStr(arr []int8) string {
	b := make([]byte, 0, len(arr))
	for _, v := range arr {
		if v == 0x00 {
			break
		}
		b = append(b, byte(v))
	}
	return string(b)
}

func ToInt(val interface{}) int {
	return ToIntDef(val, -1)
}

func ToIntDef(val interface{}, def int) int {
	if b, s := IsString(val); b {
		v, err := strconv.Atoi(s)
		if nil == err {
			return v
		}
	}
	switch i := val.(type) {
	case float32:
		return int(i)
	case float64:
		return int(i)
	case int:
		return i
	case int8:
		return int(i)
	case int16:
		return int(i)
	case int32:
		return int(i)
	case int64:
		return int(i)
	case uint:
		return int(i)
	case uint8:
		return int(i)
	case uint16:
		return int(i)
	case uint32:
		return int(i)
	case uint64:
		return int(i)
	}

	return def
}

func ToInt64(val interface{}) int64 {
	return ToInt64Def(val, -1.0)
}

func ToInt64Def(val interface{}, defVal int64) int64 {
	switch i := val.(type) {
	case float32:
		return int64(i)
	case float64:
		return int64(i)
	case int:
		return int64(i)
	case int8:
		return int64(i)
	case int16:
		return int64(i)
	case int32:
		return int64(i)
	case int64:
		return i
	case uint8:
		return int64(i)
	case uint16:
		return int64(i)
	case uint32:
		return int64(i)
	case uint64:
		return int64(i)
	case string:
		v, err := strconv.ParseInt(i, 10, 64)
		if nil == err {
			return v
		}
	}
	return defVal
}

func ToInt32(val interface{}) int32 {
	return ToInt32Def(val, -1)
}

func ToInt32Def(val interface{}, defVal int32) int32 {
	switch i := val.(type) {
	case float32:
		return int32(i)
	case float64:
		return int32(i)
	case int:
		return int32(i)
	case int8:
		return int32(i)
	case int16:
		return int32(i)
	case int32:
		return i
	case int64:
		return int32(i)
	case uint8:
		return int32(i)
	case uint16:
		return int32(i)
	case uint32:
		return int32(i)
	case uint64:
		return int32(i)
	case string:
		v, err := strconv.ParseInt(i, 10, 32)
		if nil == err {
			return int32(v)
		}
	}
	return defVal
}

func ToInt8(val interface{}) int8 {
	return ToInt8Def(val, -1)
}

func ToInt8Def(val interface{}, defVal int8) int8 {
	switch i := val.(type) {
	case float32:
		return int8(i)
	case float64:
		return int8(i)
	case int:
		return int8(i)
	case int8:
		return i
	case int16:
		return int8(i)
	case int32:
		return int8(i)
	case int64:
		return int8(i)
	case uint8:
		return int8(i)
	case uint16:
		return int8(i)
	case uint32:
		return int8(i)
	case uint64:
		return int8(i)
	case string:
		v, err := strconv.ParseInt(i, 10, 8)
		if nil == err {
			return int8(v)
		}
	}
	return defVal
}

func ToInt16(val interface{}) int16 {
	return ToInt16Def(val, -1)
}

func ToInt16Def(val interface{}, defVal int16) int16 {
	switch i := val.(type) {
	case float32:
		return int16(i)
	case float64:
		return int16(i)
	case int:
		return int16(i)
	case int8:
		return int16(i)
	case int16:
		return i
	case int32:
		return int16(i)
	case int64:
		return int16(i)
	case uint8:
		return int16(i)
	case uint16:
		return int16(i)
	case uint32:
		return int16(i)
	case uint64:
		return int16(i)
	case string:
		v, err := strconv.ParseInt(i, 10, 16)
		if nil == err {
			return int16(v)
		}
	}
	return defVal
}

func ToFloat32(val interface{}) float32 {
	return ToFloat32Def(val, -1)
}

func ToFloat32Def(val interface{}, defVal float32) float32 {
	if b, s := IsString(val); b {
		v, err := strconv.ParseFloat(s, 32)
		if nil == err {
			return float32(v)
		}
	}
	switch i := val.(type) {
	case float32:
		return i
	case float64:
		return float32(i)
	case int:
		return float32(i)
	case int8:
		return float32(i)
	case int16:
		return float32(i)
	case int32:
		return float32(i)
	case int64:
		return float32(i)
	case uint8:
		return float32(i)
	case uint16:
		return float32(i)
	case uint32:
		return float32(i)
	case uint64:
		return float32(i)
	}
	return defVal
}

func ToFloat64(val interface{}) float64 {
	return ToFloat64Def(val, -1.0)
}

func ToFloat64Def(val interface{}, defVal float64) float64 {
	switch i := val.(type) {
	case float32:
		return float64(i)
	case float64:
		return i
	case int:
		return float64(i)
	case int8:
		return float64(i)
	case int16:
		return float64(i)
	case int32:
		return float64(i)
	case int64:
		return float64(i)
	case uint8:
		return float64(i)
	case uint16:
		return float64(i)
	case uint32:
		return float64(i)
	case uint64:
		return float64(i)
	case string:
		v, err := strconv.ParseFloat(i, 64)
		if nil == err {
			return v
		}
	}
	return defVal
}

func ToBool(val interface{}) bool {
	if v, b := val.(bool); b {
		return v
	}
	if b, s := IsString(val); b {
		v, err := strconv.ParseBool(s)
		if nil == err {
			return v
		}
	}
	if a, b := val.([]byte); b {
		if len(a) > 1 {
			v, err := strconv.ParseBool(string(a))
			if nil == err {
				return v
			}
		} else {
			if a[0] == 0 {
				return false
			} else {
				return true
			}
		}
	}
	return false
}

func ToMap(val interface{}) map[string]interface{} {
	if b, s := IsString(val); b {
		m, err := stringToMap(s)
		if nil == err {
			return m
		}
	}
	if b, _ := IsMap(val); b {
		return toMap(val)
	}

	if m, err := stringToMap(ToString(val)); nil == err {
		return m
	}

	return nil
}

func ToMapOfString(val interface{}) map[string]string {
	if b, s := IsString(val); b {
		m, err := stringToMap(s)
		if nil == err {
			return toMapOfString(m)
		}
	}
	if b, _ := IsMap(val); b {
		return toMapOfString(val)
	}
	return nil
}

func ToMapOfStringArray(val interface{}) map[string][]string {
	if m, b := val.(map[string][]string); b {
		return m
	}

	// warning: this change the pointer to original object
	data, err := json.Marshal(val)
	if nil == err {
		var m map[string][]string
		err = json.Unmarshal(data, &m)
		if nil == err {
			return m
		}
	}
	return nil
}

func ForceMap(val interface{}) map[string]interface{} {
	m := ToMap(val)
	if nil == m {
		m = toMap(val)
	}
	if nil == m {
		text := strings.ReplaceAll(ToString(val), "'", "\"")
		m, _ = stringToMap(text)
	}
	if nil == m {
		m = make(map[string]interface{})
	}
	return m
}

func ForceMapOfString(val interface{}) map[string]string {
	m := ToMapOfString(val)
	if nil == m {
		return toMapOfString(val)
	}
	return m
}

func IsString(val interface{}) (bool, string) {
	v, vv := val.(string)
	if vv {
		return true, v
	}
	return false, ""
}

func IsInt(val interface{}) (bool, int) {
	v, vv := val.(int)
	if vv {
		return true, v
	}
	return false, 0
}

func IsBool(val interface{}) (bool, bool) {
	v, vv := val.(bool)
	if vv {
		return true, v
	}
	return false, false
}

func IsFloat32(val interface{}) (bool, float32) {
	v, vv := val.(float32)
	if vv {
		return true, v
	}
	return false, 0
}

func IsFloat64(val interface{}) (bool, float64) {
	v, vv := val.(float64)
	if vv {
		return true, v
	}
	return false, 0
}

func IsNumeric(val interface{}) (bool, float64) {
	switch i := val.(type) {
	case float32:
		return true, float64(i)
	case float64:
		return true, i
	case int:
		return true, float64(i)
	case int8:
		return true, float64(i)
	case int16:
		return true, float64(i)
	case int32:
		return true, float64(i)
	case int64:
		return true, float64(i)
	case string:
		v, err := strconv.ParseFloat(i, 64)
		if nil == err {
			return true, v
		}
	}
	return false, 0
}

func IsArray(val interface{}) (bool, reflect.Value) {
	rt := reflect.ValueOf(val)
	switch rt.Kind() {
	case reflect.Slice:
		return true, rt
	case reflect.Array:
		return true, rt
	default:
		return false, rt
	}
}

func IsMap(val interface{}) (bool, reflect.Value) {
	rt := reflect.ValueOf(val)
	switch rt.Kind() {
	case reflect.Map:
		return true, rt
	case reflect.Ptr:
		return IsMap(rt.Elem().Interface())
	default:
		return false, rt
	}
}

func IsStruct(val interface{}) (bool, reflect.Value) {
	rt := reflect.ValueOf(val)
	switch rt.Kind() {
	case reflect.Struct:
		return true, rt
	case reflect.Ptr:
		return IsStruct(rt.Elem().Interface())
	default:
		return false, rt
	}
}

func Equals(val1, val2 interface{}) bool {
	if val1 == val2 {
		return true
	}

	if b, v := IsString(val1); b {
		return v == ToString(val2)
	}
	if b, v := IsInt(val1); b {
		return v == ToInt(val2)
	}
	if b, v := IsFloat32(val1); b {
		return v == ToFloat32(val2)
	}
	if b, v := IsFloat64(val1); b {
		return v == ToFloat64(val2)
	}
	if b, v := IsBool(val1); b {
		return v == ToBool(val2)
	}

	return false
}

func NotEquals(val1, val2 interface{}) bool {
	if val1 == val2 {
		return true
	}

	if b, v := IsString(val1); b {
		return v != ToString(val2)
	}
	if b, v := IsInt(val1); b {
		return v != ToInt(val2)
	}
	if b, v := IsFloat32(val1); b {
		return v != ToFloat32(val2)
	}
	if b, v := IsFloat64(val1); b {
		return v != ToFloat64(val2)
	}
	if b, v := IsBool(val1); b {
		return v != ToBool(val2)
	}

	return false
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func toArray(args ...interface{}) []interface{} {
	response := make([]interface{}, 0)
	for _, val := range args {
		aa, tt := IsArray(val)
		if aa {
			for i := 0; i < tt.Len(); i++ {
				v := tt.Index(i).Interface()
				response = append(response, v)
			}
		} else {
			response = append(response, val)
		}
	}
	return response
}

func toArrayOfString(args ...interface{}) []string {
	response := make([]string, 0)
	for _, val := range args {
		b, tt := IsArray(val)
		if b {
			for i := 0; i < tt.Len(); i++ {
				v := tt.Index(i).Interface()
				response = append(response, ToString(v))
			}
		} else {
			response = append(response, ToString(val))
		}
	}

	return response
}

func toArrayOfInt(val []interface{}) []int {
	result := make([]int, 0)
	for _, v := range val {
		result = append(result, ToInt(v))
	}
	return result
}

func toArrayOfInt8(val []interface{}) []int8 {
	result := make([]int8, 0)
	for _, v := range val {
		result = append(result, ToInt8(v))
	}
	return result
}

func toArrayOfInt16(val []interface{}) []int16 {
	result := make([]int16, 0)
	for _, v := range val {
		result = append(result, ToInt16(v))
	}
	return result
}

func toArrayOfInt32(val []interface{}) []int32 {
	result := make([]int32, 0)
	for _, v := range val {
		result = append(result, ToInt32(v))
	}
	return result
}

func toArrayOfInt64(val []interface{}) []int64 {
	result := make([]int64, 0)
	for _, v := range val {
		result = append(result, ToInt64(v))
	}
	return result
}

func toArrayOfFloat32(val []interface{}) []float32 {
	result := make([]float32, 0)
	for _, v := range val {
		result = append(result, ToFloat32(v))
	}
	return result
}

func toArrayOfFloat64(val []interface{}) []float64 {
	result := make([]float64, 0)
	for _, v := range val {
		result = append(result, ToFloat64(v))
	}
	return result
}

func tryToArrayOfByte(val interface{}) []byte {
	if v, b := val.([]uint8); b {
		return v
	} else if v, b := val.([]byte); b {
		return v
	} else if v, b := val.(string); b {
		return []byte(v)
	} else if v, b := val.(bool); b {
		if v {
			return []byte{1}
		} else {
			return []byte{0}
		}
	} else {
		return []byte(ToString(val))
	}
	return nil
}

func toArrayOfByte(args interface{}) []byte {
	if nil != args {
		t := tryToArrayOfByte(args)
		if nil != t {
			return t
		}
		refVal := reflect.ValueOf(args)
		refKind := refVal.Kind()
		switch refKind {
		case reflect.Array, reflect.Slice:
			response := make([]byte, 0)
			for i := 0; i < refVal.Len(); i++ {
				t := tryToArrayOfByte(args)
				if nil != t {
					response = append(response, t...)
				}
			}
			return response
		default:
		}
	}
	return make([]byte, 0)
}

func toMap(val interface{}) map[string]interface{} {
	if m, b := val.(map[string]interface{}); b {
		return m
	}

	// warning: this change the pointer to original object
	data, err := json.Marshal(val)
	if nil == err {
		var m map[string]interface{}
		err = json.Unmarshal(data, &m)
		if nil == err {
			return m
		}
	}

	if b, _ := IsArray(val); b {
		arr := ToArray(val)
		m := make(map[string]interface{})
		for i, item := range arr {
			m[fmt.Sprintf("%v", i)] = item
		}
		return m
	}

	return nil
}

func toMapOfString(val interface{}) map[string]string {
	if m, b := val.(map[string]string); b {
		return m
	}

	// warning: this change the pointer to original object
	data, err := json.Marshal(val)
	if nil == err {
		var m map[string]string
		err = json.Unmarshal(data, &m)
		if nil == err {
			return m
		}
	}

	return nil
}

func stringToMap(text string) (map[string]interface{}, error) {
	var response map[string]interface{}
	if len(text) > 0 {
		if isValidJsonObject(text) {
			err := json.Unmarshal([]byte(text), &response)
			if nil != err {
				return nil, err
			}
			return response, nil
		} else if array, b := isValidJsonArray(text); b {
			return toMap(array), nil
		} else if strings.Index(text, "=") > 0 {
			uri, err := url.Parse("?" + text)
			if nil == err {
				query := uri.Query()
				if nil != query && len(query) > 0 {
					response = make(map[string]interface{})
					for k, v := range query {
						if len(v) == 1 {
							value := v[0]
							if isValidJsonObject(value) {
								item := map[string]interface{}{}
								err := json.Unmarshal([]byte(value), &item)
								if nil == err {
									response[k] = item
								}
							} else {
								response[k] = value
							}
						}
					}
				}
			} else {
				return nil, err
			}
		}
	}
	return response, nil
}

func isValidJsonObject(text string) bool {
	var js map[string]interface{}
	return json.Unmarshal([]byte(text), &js) == nil
}

func isValidJsonArray(text string) ([]interface{}, bool) {
	var js []interface{}
	err := json.Unmarshal([]byte(text), &js)
	return js, nil == err
}
