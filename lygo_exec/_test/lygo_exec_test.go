package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"fmt"
	"log"
	"runtime"
	"testing"
	"time"
)

func TestCmd(t *testing.T) {
	var out []byte
	var err error
	if runtime.GOOS == "windows" {
		out, err = lygo_exec.Run("tasklist")
	} else {
		out, err = lygo_exec.Run("ls", "-lah")
	}
	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}
	fmt.Println(string(out))
}



func TestDocx(t *testing.T) {
	err := lygo_exec.Open("./simple.docx")
	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}
}

func TestMultiWriter(t *testing.T) {

	cmd, comboBuffer := lygo_exec.CommandCombinedOut("ls", "-lah")

	// var stdoutBuf, stderrBuf bytes.Buffer
	// cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	// cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)

	err := cmd.Run()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}

	// outStr, errStr := string(stdoutBuf.Bytes()), string(stderrBuf.Bytes())
	// fmt.Printf("\nout:\n%s\nerr:\n%s\n", outStr, errStr)

	fmt.Println(comboBuffer.String())
}

func TestCommand(t *testing.T) {

	cmd, stdout, stderr := lygo_exec.Command("ls", "-lah")

	err := cmd.Run()
	if err != nil {
		log.Fatalf("Command() failed with %s\n", err)
	}

	fmt.Println("OUT\n", stdout.String())
	fmt.Println("ERR\n", stderr.String())
}

func TestBackground(t *testing.T) {

	bytes, err := lygo_exec.RunBackground("ls", "-lah")

	if err != nil {
		log.Fatalf("RunBackground() failed with %s\n", err)
	}

	fmt.Println(string(bytes))
}

func TestWait(t *testing.T) {
	cmd, err := lygo_exec.Start("open", "-a", "Terminal")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	p := cmd.Process
	if err := p.Kill();nil!=err{
		t.Error(err)
		t.FailNow()
	}

	if err := cmd.Wait(); err == nil {
		t.Error("Expected error \"signal: killed\"")
		t.FailNow()
	}


}

func TestRunWebServer(t *testing.T) {
	cmd, buf := lygo_exec.CommandCombinedOut("./test_fiber")
	err:= cmd.Start()
	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}

	// cmd.Wait() // cannot wait because server is locking

	// wait a while server starts
	time.Sleep(10*time.Second)
	fmt.Println(buf.String())

	// close the process
	err = cmd.Process.Kill()
	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}

}