package program

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/libreoffice"
	"fmt"
	"testing"
)

func TestLibreOfficeIsInstalled(t *testing.T) {
	cmd := libreoffice.NewCommand()
	installed := cmd.IsInstalled()
	if !installed {
		t.Error("NOT INSTALLED")
		t.FailNow()
	}
}

func TestLibreOfficeOpen(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	err := cmd.OpenDoc("./simpledoc.doc")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestLibreOfficeCmdHelp(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdHelp()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdVersion(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdVersion()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdConvert(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdConvertTo("./simpledoc.doc", "./out/simpledoc.docx")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdConvertToTxt(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdConvertToDir("./simpledoc.doc", "txt", "./out")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdConvertToPdf(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")
	cmd.SetDirOut("./out")

	response, err := cmd.CmdConvertToPdf("./simpledoc.doc")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)

	response, err = cmd.CmdConvertToEpub("./simpledoc.doc")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdPrint(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdPrint("./simpledoc.doc")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestLibreOfficeCmdCat(t *testing.T) {
	cmd := libreoffice.NewCommand()
	cmd.SetDir("./")

	response, err := cmd.CmdCat("./simpledoc.doc")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}