package program

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/npm"
	"context"
	"fmt"
	"os"
	"os/exec"
	"testing"
	"time"
)

func TestDirect(t *testing.T) {
	ctx, _ := context.WithCancel(context.Background())
	cmd := exec.CommandContext(ctx, "npm", "init", "-h")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()
	if nil != err {
		t.Error("START", err)
		t.FailNow()
	}

	err = cmd.Wait()
	if nil != err {
		t.Error("WAIT", err)
		t.FailNow()
	}
}

func TestNpmVersion(t *testing.T) {
	v, err := npm.GetVersion()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(v)
}

func TestNpmHelp(t *testing.T) {
	cmd := npm.NewCommand()
	help, err := cmd.Help()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(help)

	help, err = cmd.HelpOn("init")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("INIT:", help)
}

func TestNpmInit(t *testing.T) {
	cmd := npm.NewCommand()
	data := new(npm.DataPackage)
	data.Author = "Gian Angelo Geminiani"
	response, err := cmd.Init(data)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestNpmInstall(t *testing.T) {
	cmd := npm.NewCommand()
	response, err := cmd.Install()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestNpmRun(t *testing.T) {
	cmd := npm.NewCommand()
	response, err := cmd.Run("build")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func TestNpmStart(t *testing.T) {
	dir := "/Users/angelogeminiani/go/src/bitbucket.org/lygo/lygo_app_guardian_ide/_guardian/_development_ui/ui-pap24"
	cmd := npm.NewCommand()
	cmd.SetDir(dir)
	response, err := cmd.RunAsync("start")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response.PidCurrent(), response.PidLatest())
	time.Sleep(20*time.Second)
	err = response.Kill()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = response.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestNpmStart2(t *testing.T) {
	dir := "/Users/angelogeminiani/go/src/bitbucket.org/lygo/lygo_app_guardian_ide/_guardian/_development_ui/ui-pap24"
	cmd := environment.NewConsoleProgramWithDir("npm", dir)
	session, err := cmd.RunWrapped("start")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(session.PidCurrent(), session.PidLatest())
	time.Sleep(20*time.Second)
	err = session.Kill()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("KILLED......")
	time.Sleep(20*time.Second)
	err = session.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
