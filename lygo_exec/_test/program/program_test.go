package program_test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/chrome"
	"fmt"
	"os"
	"testing"
)

func TestPythonVersion(t *testing.T) {
	console_program.SetPythonCommand("python2")
	v, err := console_program.GetPythonVersion()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(v)
}

func TestPhpVersion(t *testing.T) {
	v, err := console_program.GetPhpVersion()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(v)
}

func TestNodeVersion(t *testing.T) {
	v, err := console_program.GetNodeVersion()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(v)
}

func TestProgram(t *testing.T) {
	//console_program.SetPythonCommand("python3")
	program := console_program.NewPythonProgram("./runnable.py")
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	program.OutWriterAppend(os.Stdout)
	program.ErrorWriterAppend(os.Stdout)

	exec, err := program.Run("arg1", "arg2")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.String())
}

func TestAsyncProgram(t *testing.T) {

	program := console_program.NewPythonProgram("./runnable.py")
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	program.OutWriterAppend(os.Stdout)
	program.ErrorWriterAppend(os.Stdout)

	exec, err := program.RunAsync("arg1", "arg2")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = exec.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.String())
}

func TestAsyncProgram2(t *testing.T) {
	console_program.SetPythonCommand("python3")
	program := console_program.NewPythonProgram("./simple.py")
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	exec, err := program.RunAsync("arg1", "arg2")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = exec.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.StdOutJson())
}

func TestPHP(t *testing.T) {
	program := console_program.NewPhpProgram("./simple.php")
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	exec, err := program.RunAsync("arg1", "arg2")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = exec.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.StdOutJson())
}

func TestNode(t *testing.T) {
	program := console_program.NewNodeProgram("./simple.js")
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	exec, err := program.RunAsync("arg1", "arg2")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = exec.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.StdOutJson())
}


func TestChrome(t *testing.T) {
	params := new(chrome.ChromeParams)
	params.AppMode = true
	params.Url = "http://www.gianangelogeminiani.me"
	params.WindowSize = "1024.0,600.0"

	program := chrome.NewChromeProgram(params)
	if nil == program {
		t.Error("Unexpected nil program")
		t.FailNow()
	}

	exec, err := program.RunAsync()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = exec.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(exec.StdOutJson())
}
