package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/vscode"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"testing"
)

func TestVSCode(t *testing.T) {
	cmd := vscode.NewCommand()
	cmd.SetDir(lygo_paths.Absolute("../../"))
	err := cmd.Open(".")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestVSCodeOpen(t *testing.T) {
	cmd := vscode.NewCommand()
	err := cmd.Open(lygo_paths.Absolute("../../"))
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestVSCodeVersion(t *testing.T) {
	cmd := vscode.NewCommand()
	v, id, a := cmd.Version()
	fmt.Println("Version:", v, id, a)
}
