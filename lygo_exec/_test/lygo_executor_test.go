package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"os"
	"testing"
)

func TestExecutor(t *testing.T) {
	// may be execution permission required
	// chmod +x ./program.sh
	command := lygo_paths.Absolute("./program.sh")
	params := []string{"this is param"}

	executor := lygo_exec.NewExecutor(command)
	executor.InputsAppend("input 1")
	executor.InputsAppend("input 2")
	executor.InputsAppend("input 3")

	executor.OutWriterAppend(os.Stdout)

	err := executor.Run(params...)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("PID: ", executor.PidCurrent())
	fmt.Println("COMMAND RUN...")

	// premature stop
	/**
	time.Sleep(1*time.Second)
	err = executor.Kill()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	**/

	err = executor.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("COMMAND END")

	fmt.Println(executor.String())
	fmt.Println(executor.StdOutLines())
	fmt.Println("Elapsed ms: ", executor.Elapsed())
}

// this is a simple test launching tesseract
// NOTE: TESSERACT MUST BE INSTALLED!!!
func TestExecutorTesseract(t *testing.T) {
	command := "tesseract"
	params := []string{"./image.jpg", "./image"}

	executor := lygo_exec.NewExecutor(command)
	executor.OutWriterAppend(os.Stdout)

	err := executor.Run(params...)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("PID: ", executor.PidCurrent())

	err = executor.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
