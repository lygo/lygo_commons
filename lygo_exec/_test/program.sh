#!/usr/bin/env bash

echo ------------------------
echo Waiting 3 seconds....
sleep 3
echo Ready
echo Write something:
read -r user_input
echo INPUT: "$user_input"
sleep 1
echo Write something else:
read -r user_input2
echo INPUT2: "$user_input2"
sleep 1
echo ------------------------