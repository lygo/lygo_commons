package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/chrome"
	"fmt"
	"os/exec"
	"testing"
)

func TestChromeApp(t *testing.T) {
	app := chrome.NewCommand()
	err := app.Open("https://www.google.com")
	if nil != err {
		t.Errorf("Err: %v", err)
		t.FailNow()
	}
	fmt.Println("CHROME ID:", app.Pid())

}

// https://peter.sh/experiments/chromium-command-line-switches/
func TestChrome(t *testing.T) {
	var out []byte
	var err error
	out, err = lygo_exec.Run("/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
		"--app=http://www.google.com",
		"--app-shell-host-window-size=800x600")

	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}
	fmt.Println(string(out))
}

func TestChrome2(t *testing.T) {
	var cmd *exec.Cmd
	var err error
	cmd, err = lygo_exec.Start("/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
		"--app=http://www.google.com")

	if nil != err {
		t.Errorf("StdErr: %v", err)
		t.FailNow()
	}
	err  = cmd.Wait()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
