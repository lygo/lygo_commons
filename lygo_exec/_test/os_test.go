package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"fmt"
	"os"
	"os/exec"
	"syscall"
	"testing"
	"time"
)

func TestOSProgram(t *testing.T) {
	cmd := "/bin/sh"
	args := []string{"-c", "i=0; while true; do echo $i> /tmp/go-test.txt;sleep 1; i=$((i+1)); done"}
	c := exec.Command(cmd, args...)
	c.Env = os.Environ()
	c.SysProcAttr = &syscall.SysProcAttr{
		// Pdeathsig: syscall.SIGTERM,
	}
	go c.Run()
	time.Sleep(10*time.Second)
	c.Process.Kill()
	fmt.Println("All Done")
	v, err := lygo_io.ReadTextFromFile("/tmp/go-test.txt")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	lygo_io.Remove("/tmp/go-test.txt")
	fmt.Println("COUNTER: ", v)
}