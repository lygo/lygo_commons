package lygo_exec

import (
	"bytes"
	"context"
	"errors"
	"os/exec"
)

// https://blog.kowalczyk.info/article/wOYk/advanced-command-execution-in-go-with-osexec.html

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func Run(cmd string, args ...string) ([]byte, error) {
	c := exec.Command(cmd, args...)
	out, err := c.CombinedOutput()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func RunBackground(cmd string, args ...string) ([]byte, error) {
	ctx := context.Background()
	c := exec.CommandContext(ctx, cmd, args...)
	out, err := c.CombinedOutput()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func RunOutput(cmd string, args ...string) (string, error) {
	c := exec.Command(cmd, args...)

	// output
	var out bytes.Buffer
	var err bytes.Buffer
	c.Stdout = &out
	c.Stderr = &err

	e := c.Run()
	if nil != e {
		return "", e
	}

	se := err.String()
	if len(se) > 0 {
		return "", errors.New(se)
	}
	return out.String(), nil
}

func Start(cmd string, args ...string) (*exec.Cmd, error) {
	c := exec.Command(cmd, args...)
	err := c.Start()
	if err != nil {
		return nil, err
	}
	return c, nil
}

// CommandCombinedOut creates a command and return command instance and output buffers (both Stdout and Stderr)
func CommandCombinedOut(cmd string, args ...string) (*exec.Cmd, *bytes.Buffer) {
	c := exec.Command(cmd, args...)

	// combined output
	var b bytes.Buffer
	c.Stdout = &b
	c.Stderr = &b

	return c, &b
}

func Command(cmd string, args ...string) (c *exec.Cmd, stdout *bytes.Buffer, stderr *bytes.Buffer) {
	c = exec.Command(cmd, args...)

	// combined output
	var out bytes.Buffer
	var err bytes.Buffer
	c.Stdout = &out
	c.Stderr = &err

	stdout = &out
	stderr = &err

	return c, stdout, stderr
}

func Open(args ...string) error {
	c := openFileCommand(args...)
	out, err := c.CombinedOutput()
	if err != nil {
		return err
	}
	if len(out) > 0 {
		// may be an error
		return errors.New(string(out))
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
