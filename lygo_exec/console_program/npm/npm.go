package npm

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"errors"
	"strings"
)

var npmCommand = "npm"

type Command struct {
	dirWork string
}

func NewCommand() *Command {
	instance := new(Command)
	instance.dirWork = lygo_paths.Absolute("./")

	return instance
}

func NewCommandWithDir(dir string) *Command {
	instance := new(Command)
	instance.dirWork = lygo_paths.Absolute(dir)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) SetDir(dir string) {
	instance.dirWork = dir
}

func (instance *Command) GetDir() string {
	return instance.dirWork
}

func (instance *Command) Version() (string, error) {
	exec, err := instance.program().Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, ".") > -1 {
		version := strings.TrimSpace(strings.ReplaceAll(response, "v", ""))
		return version, nil
	}
	return "", errors.New(exec.StdOut())
}

func (instance *Command) Help() (string, error) {
	if IsInstalled() {
		exec, err := instance.program().Run("--help")
		if nil != err && err.Error() != "exit status 1" {
			return "", err
		}
		if nil != exec {
			return exec.StdOut(), nil
		}
		return "", err
	}
	return "", environment.NotInstalledError
}

func (instance *Command) HelpOn(command string) (string, error) {
	if IsInstalled() {
		exec, err := instance.program().Run(command, "-h")
		if nil != err {
			return "", err
		}
		return exec.StdOut(), nil
	}
	return "", environment.NotInstalledError
}

func (instance *Command) Init(data *DataPackage) (string, error) {
	return instance.InitFromTemplate(tplPackage, data)
}

func (instance *Command) InitFromTemplate(tpl string, data *DataPackage) (string, error) {
	if len(data.Name) == 0 {
		data.Name = "program"
	}
	if len(data.License) == 0 {
		data.License = "MIT"
	}
	if len(data.Version) == 0 {
		data.Version = "1.0.0"
	}
	if len(data.Main) == 0 {
		data.Main = "index.js"
	}
	filename := lygo_paths.Absolute(lygo_paths.Concat(instance.dirWork, "package.json"))
	if b, _ := lygo_paths.Exists(filename); b {
		return filename, lygo_errors.Prefix(errors.New("file_already_exists"), filename)
	}
	text, err := MergeTpl(tpl, data)
	if nil != err {
		return "", err
	}
	_, err = lygo_io.WriteTextToFile(text, filename)
	return filename, err
}

func (instance *Command) Install() (string, error) {
	p := instance.program()
	return getResponse(p.Run("install"))
}

func (instance *Command) Run(command string) (string, error) {
	p := instance.program()
	return getResponse(p.Run("run", command))
}

func (instance *Command) RunAsync(command string) (*environment.ConsoleProgramSession, error) {
	p := instance.program()
	return p.RunAsync("run", command)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) program() *environment.ConsoleProgram {
	return environment.NewConsoleProgramWithDir(npmCommand, instance.dirWork)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func SetNpmCommand(cmd string) {
	if len(cmd) > 0 {
		npmCommand = cmd
	}
}

func IsInstalled() bool {
	cmd := NewCommand()
	_, err := cmd.Version()
	return nil == err
}

func GetVersion() (string, error) {
	cmd := NewCommand()
	return cmd.Version()
}

func GetHelp() (string, error) {
	cmd := NewCommand()
	return cmd.Help()
}

func getResponse(exec *environment.ConsoleProgramSession, err error) (string, error) {
	if nil != err {
		message := err.Error()
		if strings.Index(message, "exit status") == -1 {
			return "", err
		}
	}
	if nil != exec {
		stderr := exec.StdErr()
		if len(stderr) > 0 {
			return "", errors.New(stderr)
		}
		return exec.StdOut(), nil
	}
	return "", err
}
