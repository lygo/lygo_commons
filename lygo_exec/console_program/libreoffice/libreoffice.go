package libreoffice

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"os"
	"os/exec"
	"path/filepath"
)

// https://askubuntu.com/questions/519082/how-to-install-libre-office-without-gui

var libreOfficeCommand string = "soffice"

type Command struct {
	dirWork string
	dirOut  string
	session *environment.ConsoleProgramSession
}

func NewCommand() *Command {
	instance := new(Command)
	instance.dirWork = lygo_paths.Absolute("./")

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) SetDir(dir string) {
	instance.dirWork = lygo_paths.Absolute(dir)
}

func (instance *Command) GetDir() string {
	return instance.dirWork
}

func (instance *Command) SetDirOut(dir string) {
	instance.dirOut = lygo_paths.Absolute(dir)
}

func (instance *Command) GetDirOut() string {
	return instance.dirOut
}

func (instance *Command) TryKill() error {
	if nil != instance && nil != instance.session {
		return instance.session.Kill()
	}
	return nil
}

func (instance *Command) Pid() int {
	if nil != instance && nil != instance.session {
		return instance.session.PidLatest()
	}
	return 0
}

func (instance *Command) OpenDoc(filename string) error {
	session, err := instance.program(filename).Run()
	if nil != err {
		return err
	}
	instance.session = session
	return err
}

func (instance *Command) IsInstalled() bool {
	version, err := instance.CmdVersion()
	if nil != err {
		return false
	}
	return len(version) > 0
}

//----------------------------------------------------------------------------------------------------------------------
//	c o m m a n d   t o o l
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) CmdHelp() (string, error) {
	session, err := instance.program().Run("--headless", "--help")
	if nil != err {
		return "", err
	}
	instance.session = session
	return instance.session.StdOut(), nil
}

func (instance *Command) CmdVersion() (string, error) {
	session, err := instance.program().Run("--headless", "--version")
	if nil != err {
		return "", err
	}
	instance.session = session
	return instance.session.StdOut(), nil
}

// CmdConvertTo run command "$ libreoffice --convert-to docx file.txt"
func (instance *Command) CmdConvertTo(sourceFile, targetFormat string) (string, error) {
	outDir := ""
	ext := lygo_paths.ExtensionName(targetFormat)
	if len(ext) == 0 {
		ext = targetFormat
	} else {
		outDir = lygo_paths.Dir(targetFormat)
		if !lygo_paths.IsAbs(outDir) {
			outDir = lygo_paths.Concat(instance.dirWork, outDir)
		}
		_ = lygo_paths.Mkdir(outDir + lygo_paths.OS_PATH_SEPARATOR)
	}
	if len(outDir) > 0 {
		// outdir
		return instance.CmdConvertToDir(sourceFile, ext, outDir)
	} else {
		return instance.CmdConvertToDir(sourceFile, ext, instance.dirOut)
	}
}

func (instance *Command) CmdConvertToDir(sourceFile, targetFormat, outDir string) (string, error) {
	program := instance.program("--headless", "--convert-to")
	var session *environment.ConsoleProgramSession
	var err error
	if len(outDir) > 0 {
		// outdir
		session, err = program.Run(targetFormat, sourceFile, "--outdir", outDir)
	} else {
		session, err = program.Run(targetFormat, sourceFile)
	}
	if nil != err {
		return "", err
	}
	instance.session = session
	return instance.session.StdOut(), nil
}

func (instance *Command) CmdConvertToTxt(sourceFile string) (string, error) {
	return instance.CmdConvertTo(sourceFile, "txt")
}

func (instance *Command) CmdConvertToDocx(sourceFile string) (string, error) {
	return instance.CmdConvertTo(sourceFile, "docx")
}

func (instance *Command) CmdConvertToEpub(sourceFile string) (string, error) {
	return instance.CmdConvertTo(sourceFile, "epub")
}

func (instance *Command) CmdConvertToPdf(sourceFile string) (string, error) {
	return instance.CmdConvertTo(sourceFile, "pdf")
}

// CmdPrint This option prints to the default printer without opening LibreOffice; it just sends the document to your printer.
func (instance *Command) CmdPrint(sourceFile string) (string, error) {
	session, err := instance.program().Run("--headless", "-p", sourceFile)
	if nil != err {
		return "", err
	}
	instance.session = session
	return instance.session.StdOut(), nil
}

// CmdCat Dump text content of the following files to console
func (instance *Command) CmdCat(sourceFile string) (string, error) {
	session, err := instance.program().Run("--headless", "--cat", sourceFile)
	if nil != err {
		return "", err
	}
	instance.session = session
	return instance.session.StdOut(), nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) program(args ...string) *environment.ConsoleProgram {
	return environment.NewConsoleProgramWithDir(libreOfficeCommand, instance.dirWork, args...)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func init() {
	libreOfficeCommand = findExecPath()
}

// findExecPath tries to find the Chrome browser somewhere in the current
// system. It performs a rather aggressive search, which is the same in all systems.
func findExecPath() string {
	for _, path := range [...]string{
		// Unix-like
		"libreoffice",
		"soffice",
		"/usr/bin/libreoffice",

		// Windows
		"libreoffice.exe", // in case PATHEXT is misconfigured
		"soffice.exe",
		`C:\Program Files (x86)\LibreOffice\program\soffice.exe`,
		`C:\Program Files\LibreOffice\program\soffice.exe`,
		filepath.Join(os.Getenv("USERPROFILE"), `AppData\Local\LibreOffice\Application\soffice.exe`),

		// Mac
		"/Applications/LibreOffice.app/Contents/MacOS/soffice",
	} {
		found, err := exec.LookPath(path)
		if err == nil {
			return found
		}
	}
	// Fall back to something simple and sensible, to give a useful error
	// message.
	return "soffice"
}
