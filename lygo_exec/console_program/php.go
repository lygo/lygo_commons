package console_program

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"errors"
	"strings"
)

var phpCommand = "php" // uses default python version

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Replace python command. ex: "python3"
func SetPhpCommand(cmd string) {
	if len(cmd) > 0 {
		phpCommand = cmd
	}
}

func NewPhpProgram(args ...interface{}) *environment.ConsoleProgram {
	var filename string
	switch len(args) {
	case 1:
		filename = lygo_conv.ToString(args[0])
	}
	return environment.NewConsoleProgramWithFile(phpCommand, filename)
}

func GetPhpVersion() (string, error) {
	program := environment.NewConsoleProgram(phpCommand)
	exec, err := program.Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, "php") == 0 {
		version := strings.TrimSpace(strings.ReplaceAll(response, "php", ""))
		return version, nil
	} else if strings.Index(response, "php") > 0 {
		nums := lygo_regex.Numbers(response)
		if len(nums) > 0 {
			return nums[0], nil
		}
	}
	return "", errors.New(exec.StdOut())
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func init() {

}
