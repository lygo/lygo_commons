package environment

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_sys"
	"bytes"
	"io"
)

//----------------------------------------------------------------------------------------------------------------------
//	ConsoleProgramSession
//----------------------------------------------------------------------------------------------------------------------

type ConsoleProgramSession struct {
	command    string
	uid        string
	filename   string
	execDir    string
	stdout     bytes.Buffer // all outputs
	stderr     bytes.Buffer // only errors
	outWriters []io.Writer
	errWriters []io.Writer
	inputs     []string

	args      []string
	executor  *lygo_exec.Executor
	pidLatest int
}

func NewProgramSession(command, uid string, inputs []string, outWriters, errWriters []io.Writer) *ConsoleProgramSession {
	instance := new(ConsoleProgramSession)
	instance.command = command
	instance.uid = uid
	instance.filename = ""
	instance.execDir = ""
	instance.inputs = inputs
	instance.outWriters = append(instance.outWriters, outWriters...)
	instance.errWriters = append(instance.errWriters, errWriters...)

	instance.outWriters = append(instance.outWriters, &instance.stdout)
	instance.errWriters = append(instance.errWriters, &instance.stdout)
	instance.errWriters = append(instance.errWriters, &instance.stderr)

	instance.args = make([]string, 0)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ConsoleProgramSession) String() string {
	if nil != instance {
		return instance.GoString()
	}
	return ""
}

func (instance *ConsoleProgramSession) GoString() string {
	if nil != instance {
		info := map[string]interface{}{
			"command":  instance.command,
			"filename": instance.filename,
			"out":      instance.StdOut(),
			"args":     instance.args,
		}
		return lygo_json.Stringify(info)
	}
	return ""
}

func (instance *ConsoleProgramSession) StdOut() string {
	if nil != instance {
		return string(instance.stdout.Bytes())
	}
	return ""
}

func (instance *ConsoleProgramSession) StdErr() string {
	if nil != instance {
		return string(instance.stderr.Bytes())
	}
	return ""
}

func (instance *ConsoleProgramSession) StdOutJson() interface{} {
	if nil != instance {
		value := instance.StdOut()
		if a, b := lygo_json.StringToArray(value); b {
			return a
		} else if o, b := lygo_json.StringToMap(value); b {
			return o
		}
		return value
	}
	return ""
}

func (instance *ConsoleProgramSession) SetFileName(filename string) {
	instance.filename = filename
}

func (instance *ConsoleProgramSession) SetDir(dir string) {
	instance.execDir = dir
}

func (instance *ConsoleProgramSession) Run(args ...string) (*ConsoleProgramSession, error) {
	if nil != instance {
		return instance.run(args...)
	}
	return nil, nil
}

func (instance *ConsoleProgramSession) RunAsync(args ...string) (*ConsoleProgramSession, error) {
	if nil != instance {
		return instance.runAsync(args...)
	}
	return nil, nil
}

func (instance *ConsoleProgramSession) Close() (err error) {
	if nil != instance {
		if nil != instance.executor {
			err = instance.executor.Kill()
		}
	}
	return
}

func (instance *ConsoleProgramSession) Kill() (err error) {
	if nil != instance {
		if nil != instance.executor {
			err = instance.executor.Kill()
		} else if instance.pidLatest>0{
			err = lygo_sys.KillProcessByPid(instance.pidLatest)
		}
	}
	return
}

func (instance *ConsoleProgramSession) Wait() (err error) {
	if nil != instance && nil != instance.executor {
		err = instance.executor.Wait()
	}
	return
}

func (instance *ConsoleProgramSession) PidCurrent() int {
	if nil != instance && nil != instance.executor {
		return instance.executor.PidCurrent()
	}
	return 0
}

func (instance *ConsoleProgramSession) PidLatest() int {
	if nil != instance {
		if nil != instance.executor {
			return instance.executor.PidLatest()
		}
		return instance.pidLatest
	}
	return 0
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ConsoleProgramSession) prepare(args ...string) (*lygo_exec.Executor, []string) {
	// create args for execution
	params := make([]string, 0)
	if len(instance.filename) > 0 {
		if b, _ := lygo_paths.IsFile(instance.filename); b {
			params = append(params, instance.filename)
		}
	}
	params = append(params, args...)

	// creates executor
	executor := lygo_exec.NewExecutorWithDir(instance.command, instance.execDir)
	for _, input := range instance.inputs {
		executor.InputsAppend(input)
	}
	for _, w := range instance.outWriters {
		executor.OutWriterAppend(w)
	}
	for _, w := range instance.errWriters {
		executor.ErrorWriterAppend(w)
	}

	instance.args = params

	return executor, params
}

func (instance *ConsoleProgramSession) run(args ...string) (*ConsoleProgramSession, error) {
	defer instance.Close()

	instance.pidLatest = 0
	executor, params := instance.prepare(args...)
	err := executor.Run(params...)
	if nil != err {
		return nil, err
	}
	// assign executor
	instance.pidLatest = executor.PidLatest()
	instance.executor = executor
	err = executor.Wait()
	instance.executor = nil

	return instance, err
}

func (instance *ConsoleProgramSession) runAsync(args ...string) (*ConsoleProgramSession, error) {
	instance.pidLatest = 0
	executor, params := instance.prepare(args...)
	err := executor.Run(params...)
	if nil != err {
		return nil, err
	}
	// assign executor
	instance.pidLatest = executor.PidLatest()
	instance.executor = executor

	go func() {
		err = executor.Wait()
		instance.executor = nil
		_ = instance.Close()
	}()

	return instance, nil
}
