package environment

import "errors"

var(
	NotExistsFileError = errors.New("not_exists_file")
	NotInstalledError = errors.New("not_installed")
)
