package environment

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_commons/lygo_sys"
	"fmt"
	"io"
	"os"
)

type ConsoleProgram struct {
	command  string
	filename string
	dir      string
	args     []string

	inputs     []string
	outWriters []io.Writer
	errWriters []io.Writer
}

func NewConsoleProgram(command string, args ...string) *ConsoleProgram {
	instance := new(ConsoleProgram)
	instance.command = command
	instance.args = args
	instance.inputs = make([]string, 0)
	instance.outWriters = make([]io.Writer, 0)
	instance.errWriters = make([]io.Writer, 0)

	return instance
}

func NewConsoleProgramWithFile(command string, filename string, args ...string) *ConsoleProgram {
	instance := NewConsoleProgram(command, args...)
	if len(filename) > 0 {
		instance.filename = lygo_paths.Absolute(filename)
	}
	return instance
}

func NewConsoleProgramWithDir(command string, dir string, args ...string) *ConsoleProgram {
	instance := NewConsoleProgram(command, args...)
	if len(dir) > 0 {
		instance.dir = lygo_paths.Absolute(dir)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ConsoleProgram) SetFileName(filename string) {
	instance.filename = filename
}

func (instance *ConsoleProgram) SetDir(dir string) {
	instance.dir = dir
}

func (instance *ConsoleProgram) InputAppend(args ...string) {
	instance.inputs = append(instance.inputs, args...)
}

func (instance *ConsoleProgram) OutWriterAppend(w io.Writer) {
	instance.outWriters = append(instance.outWriters, w)
}

func (instance *ConsoleProgram) ErrorWriterAppend(w io.Writer) {
	instance.errWriters = append(instance.errWriters, w)
}

func (instance *ConsoleProgram) Run(args ...string) (*ConsoleProgramSession, error) {
	if nil != instance {
		session, err := instance.createSession()
		if nil == err {
			// args are in right order
			runArgs := make([]string, 0)
			runArgs = append(runArgs, instance.args...)
			runArgs = append(runArgs, args...)
			return session.Run(runArgs...)
		} else {
			return nil, err
		}
	}
	return nil, nil
}

func (instance *ConsoleProgram) RunAsync(args ...string) (*ConsoleProgramSession, error) {
	if nil != instance {
		session, err := instance.createSession()
		if nil == err {
			args = append(args, instance.args...)
			return session.RunAsync(args...)
		} else {
			return nil, err
		}
	}
	return nil, nil
}

// RunWrapped run program wrapped into runnable file launcher.
func (instance *ConsoleProgram) RunWrapped(args ...string) (*ConsoleProgramSession, error) {
	if nil != instance {
		filename, err := CreateRunnableFile(instance.dir, instance.command, args...)
		if nil != err {
			return nil, err
		}
		uid := lygo_rnd.Uuid() // session
		session := NewProgramSession(filename, uid, instance.inputs, instance.outWriters, instance.errWriters)
		session.SetDir(lygo_paths.Dir(filename))
		_, err = session.RunAsync()
		return session, err
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ConsoleProgram) createSession() (*ConsoleProgramSession, error) {
	if nil != instance {
		if len(instance.filename) > 0 {
			if b, err := lygo_paths.Exists(instance.filename); !b {
				if nil != err {
					return nil, err
				}
				return nil, NotExistsFileError
			}
		}
		uid := lygo_rnd.Uuid() // session
		session := NewProgramSession(instance.command, uid, instance.inputs, instance.outWriters, instance.errWriters)
		if len(instance.filename) > 0 {
			session.SetFileName(lygo_paths.Absolute(instance.filename))
		}
		if len(instance.dir) > 0 {
			session.SetDir(lygo_paths.Absolute(instance.dir))
		}

		return session, nil
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func CreateRunnableFile(dir string, cmd string, params ...string) (string, error) {
	if len(dir) == 0 {
		dir = "./"
	}
	dir = lygo_paths.Absolute(dir)
	ext := ".sh"
	if lygo_sys.IsWindows() {
		ext = ".bat"
	}
	filename := lygo_paths.Concat(dir, "_runnable"+ext)
	content := fmt.Sprintf("#!/bin/sh\n%v", cmd)
	if len(params) > 0 {
		content += " "
		for i, param := range params {
			if i > 0 {
				content += " "
			}
			content += param
		}
	}
	_, err := lygo_io.WriteTextToFile(content, filename)
	if nil != err {
		return "", err
	}
	err = os.Chmod(filename, 0755)
	return filename, err
}
