package vscode

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var vscodeCommand = "code"

type Command struct {
	dirWork string
}

func NewCommand() *Command {
	instance := new(Command)
	instance.dirWork = lygo_paths.Absolute("./")

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) SetDir(dir string) {
	instance.dirWork = dir
}

func (instance *Command) GetDir() string {
	return instance.dirWork
}

func (instance *Command) Open(path string) error {
	if nil != instance {
		_, err := instance.program().Run(path)
		if nil != err {
			return err
		}
	}
	return nil
}

func (instance *Command) Version() (version, commitId, architecture string) {
	if nil != instance {
		session, _ := instance.program().Run("--version")
		if nil != session {
			out := session.StdOut()
			tokens := strings.Split(out, "\n")
			return lygo.Arrays.GetAt(tokens, 0, "").(string),
			lygo.Arrays.GetAt(tokens, 1, "").(string),
				lygo.Arrays.GetAt(tokens, 2, "").(string)
		}
	}
	return "", "", ""
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Command) program() *environment.ConsoleProgram {
	return environment.NewConsoleProgramWithDir(vscodeCommand, instance.dirWork)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func init() {
	vscodeCommand = findExecPath()
}

func findExecPath() string {
	for _, path := range [...]string{
		// Unix-like
		"code",
		"/usr/bin/code",

		// Windows
		"code.exe", // in case PATHEXT is misconfigured
		`C:\Program Files (x86)\Microsoft VS Code\chrome.exe`,
		`C:\Program Files\Microsoft VS Code\chrome.exe`,
		filepath.Join(os.Getenv("USERPROFILE"), `AppData\Local\Programs\Microsoft VS Code\code.exe`),

		// Mac
		"/Applications/Visual Studio Code.app/Contents/Resources/app/bin/code",
	} {
		found, err := exec.LookPath(path)
		if err == nil {
			return found
		}
	}
	// Fall back to something simple and sensible, to give a useful error
	// message.
	return "code"
}
