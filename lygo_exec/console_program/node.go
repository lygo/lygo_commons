package console_program

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"errors"
	"strings"
)

var nodeCommand = "node" // uses default python version

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Replace python command. ex: "python3"
func SetNodeCommand(cmd string) {
	if len(cmd) > 0 {
		nodeCommand = cmd
	}
}

func NewNodeProgram(args ...interface{}) *environment.ConsoleProgram {
	var filename string
	switch len(args) {
	case 1:
		filename = lygo_conv.ToString(args[0])
	}
	return environment.NewConsoleProgramWithFile(nodeCommand, filename)
}

func GetNodeVersion() (string, error) {
	program := environment.NewConsoleProgram(nodeCommand)
	exec, err := program.Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, "v")==0{
		version := strings.TrimSpace(strings.ReplaceAll(response, "v", ""))
		return version, nil
	}
	return "", errors.New(exec.StdOut())
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func init() {

}
