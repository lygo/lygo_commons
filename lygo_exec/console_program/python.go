package console_program

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"errors"
	"strings"
)

var pythonCommand = "python" // uses default python version

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Replace python command. ex: "python3"
func SetPythonCommand(cmd string) {
	if len(cmd) > 0 {
		pythonCommand = cmd
	}
}

func NewPythonProgram(args ...interface{}) *environment.ConsoleProgram {
	var filename string
	switch len(args) {
	case 1:
		filename = lygo_conv.ToString(args[0])
	}
	return environment.NewConsoleProgramWithFile(pythonCommand, filename)
}

func GetPythonVersion() (string, error) {
	program := environment.NewConsoleProgram(pythonCommand)
	exec, err := program.Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, "python")>-1{
		version := strings.TrimSpace(strings.ReplaceAll(response, "python", ""))
		return version, nil
	}
	return "", errors.New(exec.StdOut())
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func init() {

}
