## Ly Go Python ##

![](icon.png)

Basic Python integration without C bindings.

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_python
```

## Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_commons
```

## Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.1
git push origin v0.1.1
```