package lygo_paths

import (
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	f i e l d s
//----------------------------------------------------------------------------------------------------------------------

const DEF_TEMP = "./_temp"
const OS_PATH_SEPARATOR = string(os.PathSeparator)

var _workspace *WorkspaceController
var _temp_root string = DEF_TEMP
var _pathSeparator = OS_PATH_SEPARATOR

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	_workspace = NewWorkspaceController()
	_workspace.Get("*").SetPath(DEF_WORKSPACE)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func GetWorkspace(name string) *Workspace {
	return _workspace.Get(name)
}

func GetWorkspacePath() string {
	return _workspace.Get("*").GetPath()
}

func SetWorkspacePath(value string) {
	_workspace.Get("*").SetPath(value)
}

func SetWorkspaceParent(value string) {
	_workspace.Get("*").SetPath(filepath.Join(Absolute(value), DEF_WORKSPACE))
}

func WorkspacePath(partial string) string {
	return _workspace.Get("*").Resolve(partial)
}

func GetTempRoot() string {
	return Absolute(_temp_root)
}

func SetTempRoot(path string) {
	_temp_root = Absolute(path)
}

func TempPath(partial string) string {
	return Concat(GetTempRoot(), partial)
}

func UserHomeDir() (string, error) {
	return os.UserHomeDir()
}

// DatePath return a date based path ex: "/2020/01/23/file.txt"
// @param root Root, starting directory
// @param partial File name
// @param level 1=Year, 2=Year/Month, 3=Year/Month/Day, 4=Year/Mont/Day/Hour, 5=Year/Mont/Day/Hour/Minute, 6=Year/Mont/Day/Hour/minute/Second
// @param autoCreate If true, creates directories
func DatePath(root, partial string, level int, autoCreate bool) string {
	if len(root) == 0 {
		root = "./"
	}
	var pattern string
	switch level {
	case 0:
		pattern = ""
	case 1:
		pattern = "yyyy"
	case 2:
		pattern = "yyyyMM"
	case 3:
		pattern = "yyyyMMdd"
	case 4:
		pattern = "yyyyMMdd/HH"
	case 5:
		pattern = "yyyyMMdd/HH/mm"
	case 6:
		pattern = "yyyyMMdd/HH/mm/ss"
	default:
		pattern = "yyyyMMdd"
	}
	path := lygo_date.FormatDate(time.Now(), pattern)
	result := Concat(Absolute(root), path, partial)
	if autoCreate {
		_ = Mkdir(result)
	}
	return result
}

func Concat(paths ...string) string {
	result := filepath.Join(paths...)
	if IsUrl(result) && strings.Index(result, "://") == -1 {
		result = strings.Replace(result, ":/", "://", 1)
	}
	return result
}

func ConcatDir(paths ...string) string {
	result := filepath.Join(paths...) + string(os.PathSeparator)
	if IsUrl(result) && strings.Index(result, "://") == -1 {
		result = strings.Replace(result, ":/", "://", 1)
	}
	return result
}

// Check if a path exists and returns a boolean value or an error if access is denied
// @param path Path to check
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// EnsureTrailingSlash add path separator to end if any.
func EnsureTrailingSlash(dir string) string {
	if strings.HasSuffix(dir, OS_PATH_SEPARATOR) {
		return dir
	}
	if len(Extension(dir)) == 0 {
		return dir + OS_PATH_SEPARATOR
	}
	return dir
}

func Absolute(path string) string {
	abs, err := filepath.Abs(path)
	if nil == err {
		return abs
	}
	return path
}

func Absolutize(path, root string) string {
	if IsAbs(path) {
		return path
	}
	if len(root) == 0 {
		return Absolute(path)
	}
	return Concat(root, path)
}

func Dir(path string) string {
	return filepath.Dir(path)
}

func Extension(path string) string {
	return filepath.Ext(path)
}

func ExtensionName(path string) string {
	return strings.Replace(Extension(path), ".", "", 1)
}

func FileName(path string, includeExt bool) string {
	if IsUrl(path) {
		uri, err := url.Parse(path)
		if nil != err {
			return ""
		}
		path := uri.Path
		if len(path) > 1 {
			ext := ExtensionName(path)
			if len(ext) > 0 {
				return FileName(path, includeExt)
			}
			return filepath.Base(uri.Path) // i.e. http://file-no-ext
		}
		return ""
	} else {
		base := filepath.Base(path)
		if includeExt {
			return base
		} else {
			ext := filepath.Ext(base)
			return strings.Replace(base, ext, "", 1)
		}
	}
}

// Creates a directory and all subdirectories if does not exists
func Mkdir(path string) (err error) {
	// ensure we have a directory
	var abs string
	if filepath.IsAbs(path) {
		abs = path
	} else {
		abs, err = filepath.Abs(path)
	}

	if nil == err {
		if IsFilePath(abs) {
			abs = filepath.Dir(abs)
		}

		if !strings.HasSuffix(abs, _pathSeparator) {
			path = abs + string(os.PathSeparator)
		} else {
			path = abs
		}

		var b bool
		if b, err = Exists(path); !b && nil == err {
			err = os.MkdirAll(path, os.ModePerm)
		}

	}

	return err
}

//IsPath get a string and check if is a valid path
func IsPath(path string) bool {
	clean := strings.Trim(path, " ")
	if strings.Contains(clean, OS_PATH_SEPARATOR) {
		return true
	}
	return false
}

//IsTemp return true if path is under "./_temp"
func IsTemp(path string) bool {
	tokens := strings.Split(path, _pathSeparator)
	temp := FileName(_temp_root, false)
	for _, token := range tokens {
		if token == temp {
			return true
		}
	}
	return false
}

func IsDir(path string) (bool, error) {
	fi, err := os.Lstat(Absolute(path))
	if nil != err {
		return false, err
	}
	return fi.Mode().IsDir(), nil
}

func IsFile(path string) (bool, error) {
	fi, err := os.Lstat(Absolute(path))
	if nil == err {
		return fi.Mode().IsRegular(), err
	} else {
		// path or file does not exists
		// just check if has extension
		if len(filepath.Ext(path)) > 0 {
			return true, nil
		}
		if strings.HasSuffix(path, _pathSeparator) {
			// is a directory
			return false, err
		}
		return true, err
	}
}

func IsFilePath(path string) bool {
	if b, _ := Exists(path); b {
		b, _ = IsFile(path)
		return b
	} else {
		if strings.HasSuffix(path, _pathSeparator) {
			// is a directory
			return false
		}
		return len(filepath.Ext(path)) > 0
	}
}

func IsDirPath(path string) bool {
	if b, _ := Exists(path); b {
		b, _ = IsDir(path)
		return b
	} else {
		return len(filepath.Ext(path)) == 0
	}
}

func IsAbs(path string) bool {
	if IsUrl(path) {
		return true
	}
	return filepath.IsAbs(path)
}

func IsUrl(path string) bool {
	return strings.Index(path, "http") == 0
}

func IsSymLink(path string) (bool, error) {
	fi, err := os.Lstat(Absolute(path))
	return fi.Mode()&os.ModeSymlink != 0, err
}

func IsHiddenFile(path string) (bool, error) {
	return isHiddenFile(path)
}

func IsSameFile(path1, path2 string) (bool, error) {
	f1, err1 := os.Lstat(Absolute(path1))
	f2, err2 := os.Lstat(Absolute(path2))
	if nil != err1 {
		return false, err1
	}
	if nil != err2 {
		return false, err2
	}
	return sameFile(f1, f2), nil
}

func IsSameFileInfo(f1, f2 os.FileInfo) bool {
	return sameFile(f1, f2)
}

func ListAll(root string) ([]string, error) {
	var response []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		response = append(response, path)
		return nil
	})
	return response, err
}

func ListFiles(root string, filter string) ([]string, error) {
	var response []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			if len(filter) == 0 {
				response = append(response, path)
			} else {
				name := filepath.Base(path)
				if len(lygo_regex.WildcardMatch(name, filter)) > 0 {
					response = append(response, path)
				}
			}
		}
		return nil
	})
	return response, err
}

func ListDir(root string) ([]string, error) {
	var response []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() && path != root {
			response = append(response, path)
		}
		return nil
	})
	return response, err
}

func ReadDirOnly(root string) ([]string, error) {
	var response []string
	infos, err := ioutil.ReadDir(root)
	for _, info := range infos {
		if info.IsDir() {
			response = append(response, info.Name())
		}
	}
	return response, err
}

func ReadFileOnly(root string) ([]string, error) {
	var response []string
	infos, err := ioutil.ReadDir(root)
	for _, info := range infos {
		if !info.IsDir() {
			response = append(response, info.Name())
		}
	}
	return response, err
}

// ReadDir reads the directory named by dirname and returns
// a list of file's and directory's name.
func ReadDir(root string) ([]string, error) {
	var response []string
	infos, err := ioutil.ReadDir(root)
	for _, info := range infos {
		response = append(response, info.Name())
	}
	return response, err
}

func TmpFileName(extension string) string {
	uuid := lygo_rnd.Uuid()
	if len(uuid) == 0 {
		uuid = "temp_file"
	}

	return uuid + ensureDot(extension)
}

func TmpFile(extension string) string {
	path := filepath.Join(_temp_root, TmpFileName(extension))
	return Absolute(path)
}

func ChangeFileName(fromPath, toFileName string) string {
	parent := filepath.Dir(fromPath)
	base := filepath.Base(fromPath)
	ext := filepath.Ext(base)
	if len(filepath.Ext(toFileName)) > 0 {
		return filepath.Join(parent, toFileName)
	}
	return filepath.Join(parent, toFileName+ext)
}

func ChangeFileNameExtension(fromPath, toFileExtension string) string {
	parent := filepath.Dir(fromPath)
	base := filepath.Base(fromPath)
	ext := filepath.Ext(base)
	name := strings.Replace(base, ext, "", 1)
	return filepath.Join(parent, name+ensureDot(toFileExtension))
}

func ChangeFileNameWithSuffix(path, suffix string) string {
	base := filepath.Base(path)
	ext := filepath.Ext(base)
	name := strings.Replace(base, ext, "", 1)
	return filepath.Join(filepath.Dir(path), name+suffix+ext)
}

func ChangeFileNameWithPrefix(path, prefix string) string {
	base := filepath.Base(path)
	return filepath.Join(filepath.Dir(path), prefix+base)
}

func Clean(pathOrUrl string) string {
	if IsUrl(pathOrUrl) {
		return CleanUrl(pathOrUrl)
	}
	return CleanPath(pathOrUrl)
}

func CleanPath(p string) string {
	return filepath.Clean(p)
}

// CleanUrl is the URL version of path.Clean, it returns a canonical URL path
// for p, eliminating . and .. elements.
//
// The following rules are applied iteratively until no further processing can
// be done:
//	1. Replace multiple slashes with a single slash.
//	2. Eliminate each . path name element (the current directory).
//	3. Eliminate each inner .. path name element (the parent directory)
//	   along with the non-.. element that precedes it.
//	4. Eliminate .. elements that begin a rooted path:
//	   that is, replace "/.." by "/" at the beginning of a path.
//
// If the result of this process is an empty string, "/" is returned
func CleanUrl(p string) string {
	// Turn empty string into "/"
	if p == "" {
		return "/"
	}

	n := len(p)
	var buf []byte

	// Invariants:
	//      reading from path; r is index of next byte to process.
	//      writing to buf; w is index of next byte to write.

	// path must start with '/'
	r := 1
	w := 1

	if p[0] != '/' {
		r = 0
		buf = make([]byte, n+1)
		buf[0] = '/'
	}

	trailing := n > 2 && p[n-1] == '/'

	// A bit more clunky without a 'lazybuf' like the path package, but the loop
	// gets completely inlined (bufApp). So in contrast to the path package this
	// loop has no expensive function calls (except 1x make)

	for r < n {
		switch {
		case p[r] == '/':
			// empty path element, trailing slash is added after the end
			r++

		case p[r] == '.' && r+1 == n:
			trailing = true
			r++

		case p[r] == '.' && p[r+1] == '/':
			// . element
			r++

		case p[r] == '.' && p[r+1] == '.' && (r+2 == n || p[r+2] == '/'):
			// .. element: remove to last /
			r += 2

			if w > 1 {
				// can backtrack
				w--

				if buf == nil {
					for w > 1 && p[w] != '/' {
						w--
					}
				} else {
					for w > 1 && buf[w] != '/' {
						w--
					}
				}
			}

		default:
			// real path element.
			// add slash if needed
			if w > 1 {
				bufApp(&buf, p, w, '/')
				w++
			}

			// copy element
			for r < n && p[r] != '/' {
				bufApp(&buf, p, w, p[r])
				w++
				r++
			}
		}
	}

	// re-append trailing slash
	if trailing && w > 1 {
		bufApp(&buf, p, w, '/')
		w++
	}

	if buf == nil {
		return p[:w]
	}
	return string(buf[:w])
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func ensureDot(extension string) string {
	if strings.Index(extension, ".") == -1 {
		extension = "." + extension
	}
	return extension
}

// internal helper to lazily create a buffer if necessary
func bufApp(buf *[]byte, s string, w int, c byte) {
	if *buf == nil {
		if s[w] == c {
			return
		}

		*buf = make([]byte, len(s))
		copy(*buf, s[:w])
	}
	(*buf)[w] = c
}
