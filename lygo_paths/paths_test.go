package lygo_paths

import (
	"fmt"
	"testing"
)

func TestConcat(t *testing.T) {
	p := "/confirm-email?mode=emailVerified&confirm_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMDUxOGZhMzNhYzA3N2Q1OTlmMmU5ZGNkYWRhYWEzNDciLCJwYXlsb2FkIjp7ImNvbmZpcm1lZCI6ZmFsc2UsInVzZXJfbmFtZSI6ImFsZXhhbmRyby5zY2FybmljY2hpYUBib3Rpa2EuYWkiLCJ1c2VyX3BzdyI6IjE5IVBhbGxpbm8hNzUifSwic2VjcmV0X3R5cGUiOiJhY2Nlc3MiLCJleHAiOjQ2ODE5OTE4MzMsImp0aSI6IjAyNWJhNzlkZmFlMDkxOGFlMTA0OTFhMGY0ZTcxZGFlIn0.9UEf-x7J0HaoNDJsC-yT860pJp-notR_lZt_5zpnPA0"
	paths := []string{"/user/file.txt", "/user/file", "./user/file.html",
		"https://www.g.com/user/dir/"}
	for _, path := range paths {
		result := Concat(path, p)
		fmt.Println(path, "+", p, "\n\t=", result)
	}
}

func TestListDir(t *testing.T) {
	dirs, err := ListDir(Absolute("../"))
	if nil!=err{
		t.Error(err)
	}
	for _, dir:=range dirs{
		fmt.Println(dir)
	}
}

func TestReadDir(t *testing.T) {
	dirs, err := ReadDir(Absolute("../"))
	if nil!=err{
		t.Error(err)
	}
	for _, dir:=range dirs{
		fmt.Println(dir)
	}
}

func TestReadDirOnly(t *testing.T) {
	dirs, err := ReadDirOnly(Absolute("../"))
	if nil!=err{
		t.Error(err)
	}
	for _, dir:=range dirs{
		fmt.Println(dir)
	}
}

func TestReadFileOnly(t *testing.T) {
	dirs, err := ReadFileOnly(Absolute("../"))
	if nil!=err{
		t.Error(err)
	}
	for _, dir:=range dirs{
		fmt.Println(dir)
	}
}

func TestDir(t *testing.T) {
	paths := []string{"/user/file.txt", "/user/file", "./user/file.html", "http://www.g.com/user/file.html"}
	for _, path := range paths {
		dir := Dir(path)
		fmt.Println(path, dir)
	}
}

func TestEnsureTrailingSlash(t *testing.T) {
	paths := []string{"/user", "/user/file", "./user/file.html", "http://www.g.com/user/file.html"}
	for _, path := range paths {
		dir := EnsureTrailingSlash(path)
		fmt.Println(path, dir)
	}
}

func TestDatePath(t *testing.T) {
	paths := []string{"/user/file.txt", "/user/file", "./user/file.html", ""}
	for _, path := range paths {
		fmt.Println(path, ":")
		for level := 0; level < 7; level++ {
			dir := DatePath("", path, level, false)
			fmt.Println("\t", level, dir)
		}
	}
}

func TestFileName(t *testing.T) {
	urls := []string{"http://domain.com/dir/file.html", "http://domain.com/", "http://domain.com/filenoext"}
	for _, url := range urls {
		name := FileName(url, true)
		fmt.Println("file name:", name)
	}
}

func TestIsTemp(t *testing.T) {
	urls := []string{"http://domain.com/dir/file.html", "http://domain.com/", "http://domain.com/filenoext",
		"http://domain.com/_temp", "_temp/dir/file.ytg"}
	for _, url := range urls {
		isTemp := IsTemp(url)
		fmt.Println("temp:", isTemp, url)
	}
}

func TestAbsolute(t *testing.T) {
	p1 := Absolute("/users/dir1/dir2/../../user.txt")
	if p1 != "/users/user.txt" {
		t.Error("expected", "/users/user.txt", "but got", p1)
		t.FailNow()
	}
}

func TestAbsolutize(t *testing.T) {
	p1 := Absolutize("./dir1/dir2/../../user.txt", "/users")
	if p1 != "/users/user.txt" {
		t.Error("expected", "/users/user.txt", "but got", p1)
		t.FailNow()
	}
}

func TestExists(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Exists(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Exists() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Exists() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExtension(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Extension(tt.args.path); got != tt.want {
				t.Errorf("Extension() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExtensionName(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExtensionName(tt.args.path); got != tt.want {
				t.Errorf("ExtensionName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetWorkspacePath(t *testing.T) {

	path1 := GetWorkspacePath()
	if path1 != Absolute("./_workspace") {
		t.Error("Bad workspace: " + path1)
		t.FailNow()
	}
	SetWorkspacePath("./space")
	path1 = GetWorkspacePath()
	if path1 != Absolute("./space") {
		t.Error("Bad workspace: " + path1)
		t.FailNow()
	}
	solved := WorkspacePath("mydir")
	if solved != Absolute("./space/mydir") {
		t.Error("Bad sub: " + solved)
		t.FailNow()
	}

	// custom
	w1 := GetWorkspace("myspace")
	path1 = w1.GetPath()
	if path1 != Absolute("./_workspace") {
		t.Error("Bad workspace: " + path1)
		t.FailNow()
	}
	w1.SetPath("./sub")
	path1 = w1.GetPath()
	if path1 != Absolute("./sub") {
		t.Error("Bad workspace: " + path1)
		t.FailNow()
	}
	solved = w1.Resolve("mydir")
	if solved != Absolute("./sub/mydir") {
		t.Error("Bad sub: " + solved)
		t.FailNow()
	}
}

func TestIsDir(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := IsDir(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsDir() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsDir() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsFile(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := IsFile(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsFile() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsSymLink(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := IsSymLink(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsSymLink() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsSymLink() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMkdir(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"test1", args{"./dir1/dir2/file.log"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Mkdir(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("Mkdir() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSetWorkspacePath(t *testing.T) {
	type args struct {
		value string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func TestTmpFile(t *testing.T) {
	type args struct {
		extension string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TmpFile(tt.args.extension); got != tt.want {
				t.Errorf("TmpFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWorkspacePath(t *testing.T) {
	type args struct {
		partial string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"aaa", args{"path1/path2/file.txt"}, "aa"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := WorkspacePath(tt.args.partial); got != tt.want {
				t.Errorf("WorkspacePath() = %v, want %v", got, tt.want)
			}
		})
	}
}
