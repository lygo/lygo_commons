package lygo_paths

import (
	"path/filepath"
	"sync"
)

const  DEF_WORKSPACE = "_workspace"


//----------------------------------------------------------------------------------------------------------------------
//	WorkspaceController
//----------------------------------------------------------------------------------------------------------------------

type WorkspaceController struct {
	repo map[string]*Workspace
	mux sync.Mutex
}

func NewWorkspaceController()*WorkspaceController{
	instance := new(WorkspaceController)
	instance.repo = make(map[string]*Workspace)

	instance.Get("*").SetPath(DEF_WORKSPACE)

	return instance
}

func (instance *WorkspaceController) Get(key string) *Workspace {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if _,b:=instance.repo[key];!b{
		w := new(Workspace)
		w.name = key
		w.SetPath(DEF_WORKSPACE)
		instance.repo[key] = w
	}
	return instance.repo[key]
}

//----------------------------------------------------------------------------------------------------------------------
//	Workspace
//----------------------------------------------------------------------------------------------------------------------

type Workspace struct {
	name string
	path string
}

func (instance *Workspace) GetPath() string {
	return instance.path
}

func (instance *Workspace) SetPath(path string) {
	instance.path = Absolute(path)
}

// Resolve get absolute path under this workspace path
func (instance *Workspace) Resolve(partial string) string {
	if filepath.IsAbs(partial) {
		return partial
	}
	return filepath.Join(instance.GetPath(), partial)
}
