package lygo_reflect

import (
	"encoding/json"
	"fmt"
	"testing"
)

type MyDoc struct {
	Name string
	Date string
}

func TestSimple(t *testing.T) {

	// works with struct
	doc := &MyDoc{
		Name: "Foo",
		Date: "25/2/2020",
	}
	name := Get(doc, "Name")
	if name != "Foo" {
		t.Errorf("Expected 'Foo', but got '%v'", name)
		t.FailNow()
	}
	if b := Set(doc, "Name", "Test"); !b {
		t.Error("Unable to set value")
		t.FailNow()
	}
	name = Get(doc, "Name")
	if name != "Test" {
		t.Errorf("Expected 'Test', but got '%v'", name)
		t.FailNow()
	}

	// works with map
	var mdoc interface{}
	mdoc = map[string]interface{}{
		"Name": "Foo",
	}
	name = Get(mdoc, "Name")
	if name != "Foo" {
		t.FailNow()
	}
	b := Set(mdoc, "Name", "Test")
	if !b {
		t.FailNow()
	}
	name = Get(mdoc, "Name")
	if name != "Test" {
		t.Errorf("Expected 'Test', but got '%v'", name)
		t.FailNow()
	}
	fmt.Println("NAME", name)

	// works with interface
	var inter interface{}
	bytes, _ := json.Marshal(doc)
	_ = json.Unmarshal(bytes, &inter)
	if b := Set(inter, "Name", "INTER-NAME"); !b {
		t.Error("Unable to set value")
		t.FailNow()
	}
	name = Get(inter, "Name")
	if name != "INTER-NAME" {
		t.Errorf("Expected 'INTER-NAME', but got '%v'", name)
		t.FailNow()
	}
	fmt.Println("NAME", name)
}
